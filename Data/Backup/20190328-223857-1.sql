-- -----------------------------
-- Think MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 3306
-- Database : teaching-m
-- 
-- Part : #1
-- Date : 2019-03-28 22:38:57
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `ocenter_config`
-- -----------------------------
DROP TABLE IF EXISTS `ocenter_config`;
CREATE TABLE `ocenter_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值说明',
  `remark` varchar(500) NOT NULL COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=1022 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `ocenter_config`
-- -----------------------------
INSERT INTO `ocenter_config` VALUES ('100', 'WEB_SITE_CLOSE', '4', '关闭站点', '1', '0:关闭,1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', '1378898976', '1379235296', '1', '1', '11');
INSERT INTO `ocenter_config` VALUES ('101', 'CONFIG_TYPE_LIST', '3', '配置类型列表', '4', '', '主要用于数据解析和页面表单的生成', '1378898976', '1379235348', '1', '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举\r\n8:多选框', '8');
INSERT INTO `ocenter_config` VALUES ('102', 'CONFIG_GROUP_LIST', '3', '配置分组', '4', '', '配置分组', '1379228036', '1384418383', '1', '1:基本\r\n2:内容\r\n3:用户\r\n4:系统\r\n5:邮件', '15');
INSERT INTO `ocenter_config` VALUES ('103', 'HOOKS_TYPE', '3', '钩子的类型', '4', '', '类型 1-用于扩展显示内容，2-用于扩展业务处理', '1379313397', '1379313407', '1', '1:视图\r\n2:控制器', '17');
INSERT INTO `ocenter_config` VALUES ('104', 'AUTH_CONFIG', '3', 'Auth配置', '4', '', '自定义Auth.class.php类配置', '1379409310', '1379409564', '1', 'AUTH_ON:1\r\nAUTH_TYPE:2', '20');
INSERT INTO `ocenter_config` VALUES ('105', 'LIST_ROWS', '0', '后台每页记录数', '2', '', '后台数据每页显示记录数', '1379503896', '1380427745', '1', '10', '24');
INSERT INTO `ocenter_config` VALUES ('107', 'CODEMIRROR_THEME', '4', '预览插件的CodeMirror主题', '4', '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '详情见CodeMirror官网', '1379814385', '1384740813', '1', 'ambiance', '13');
INSERT INTO `ocenter_config` VALUES ('108', 'DATA_BACKUP_PATH', '1', '数据库备份根路径', '4', '', '路径必须以 / 结尾', '1381482411', '1381482411', '1', './Data/Backup', '16');
INSERT INTO `ocenter_config` VALUES ('109', 'DATA_BACKUP_PART_SIZE', '0', '数据库备份卷大小', '4', '', '该值用于限制压缩后的分卷最大长度。单位：B；建议设置20M', '1381482488', '1381729564', '1', '20971520', '18');
INSERT INTO `ocenter_config` VALUES ('110', 'DATA_BACKUP_COMPRESS', '4', '数据库备份文件是否启用压缩', '4', '0:不压缩\r\n1:启用压缩', '压缩备份文件需要PHP环境支持gzopen,gzwrite函数', '1381713345', '1381729544', '1', '1', '22');
INSERT INTO `ocenter_config` VALUES ('111', 'DATA_BACKUP_COMPRESS_LEVEL', '4', '数据库备份文件压缩级别', '4', '1:普通\r\n4:一般\r\n9:最高', '数据库备份文件的压缩级别，该配置在开启压缩时生效', '1381713408', '1381713408', '1', '9', '25');
INSERT INTO `ocenter_config` VALUES ('112', 'DEVELOP_MODE', '4', '开启开发者模式', '4', '0:关闭\r\n1:开启', '是否开启开发者模式，部分后台功能必须在开发者模式下才生效，重新设置后清空缓存生效', '1383105995', '1383291877', '1', '0', '26');
INSERT INTO `ocenter_config` VALUES ('113', 'ALLOW_VISIT', '3', '不受限控制器方法', '0', '', '', '1386644047', '1386644741', '1', '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', '2');
INSERT INTO `ocenter_config` VALUES ('114', 'DENY_VISIT', '3', '超管专限控制器方法', '0', '', '仅超级管理员可访问的控制器方法', '1386644141', '1386644659', '1', '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', '3');
INSERT INTO `ocenter_config` VALUES ('115', 'ADMIN_ALLOW_IP', '2', '后台允许访问IP', '4', '', '多个用逗号分隔，如果不配置表示不限制IP访问', '1387165454', '1387165553', '1', '', '27');
INSERT INTO `ocenter_config` VALUES ('116', 'SHOW_PAGE_TRACE', '4', '是否显示页面Trace', '4', '0:关闭\r\n1:开启', '是否显示页面Trace信息', '1387165685', '1387165685', '1', '0', '7');
INSERT INTO `ocenter_config` VALUES ('117', 'MAIL_TYPE', '4', '邮件类型', '5', '1:SMTP 模块发送\r\n2:mail() 函数发送', '如果您选择了采用服务器内置的 Mail 服务，您不需要填写下面的内容', '1388332882', '1388931416', '1', '1', '0');
INSERT INTO `ocenter_config` VALUES ('118', 'MAIL_SMTP_HOST', '1', 'SMTP 服务器', '5', '', 'SMTP服务器', '1388332932', '1388332932', '1', '', '0');
INSERT INTO `ocenter_config` VALUES ('119', 'MAIL_SMTP_PORT', '0', 'SMTP服务器端口', '5', '', '默认25', '1388332975', '1388332975', '1', '25', '0');
INSERT INTO `ocenter_config` VALUES ('120', 'MAIL_SMTP_USER', '1', 'SMTP服务器用户名', '5', '', '填写完整用户名', '1388333010', '1388333010', '1', '', '0');
INSERT INTO `ocenter_config` VALUES ('121', 'MAIL_SMTP_PASS', '6', 'SMTP服务器密码', '5', '', '填写您的密码', '1388333057', '1389187088', '1', '', '0');
INSERT INTO `ocenter_config` VALUES ('122', 'MAIL_USER_PASS', '5', '密码找回模板', '0', '', '支持HTML代码', '1388583989', '1388672614', '1', '密码找回111223333555111', '0');
INSERT INTO `ocenter_config` VALUES ('123', 'PIC_FILE_PATH', '1', '图片文件保存根目录', '4', '', '图片文件保存根目录./目录/', '1388673255', '1388673255', '1', './Uploads/', '0');
INSERT INTO `ocenter_config` VALUES ('124', 'COUNT_DAY', '0', '后台首页统计用户增长天数', '0', '', '默认统计最近半个月的用户数增长情况', '1420791945', '1420876261', '1', '7', '0');
INSERT INTO `ocenter_config` VALUES ('126', 'USER_NAME_BAOLIU', '1', '保留用户名和昵称', '3', '', '禁止注册用户名和昵称，包含这些即无法注册,用\" , \"号隔开，用户只能是英文，下划线_，数字等', '1388845937', '1388845937', '1', '管理员,测试,admin,垃圾', '0');
INSERT INTO `ocenter_config` VALUES ('128', 'VERIFY_OPEN', '8', '验证码配置', '4', 'reg:注册显示\r\nlogin:登陆显示\r\nreset:密码重置', '验证码配置', '1388500332', '1405561711', '1', '', '0');
INSERT INTO `ocenter_config` VALUES ('129', 'VERIFY_TYPE', '4', '验证码类型', '4', '1:中文\r\n2:英文\r\n3:数字\r\n4:英文+数字', '验证码类型', '1388500873', '1405561731', '1', '4', '0');
INSERT INTO `ocenter_config` VALUES ('130', 'NO_BODY_TLE', '2', '空白说明', '2', '', '空白说明', '1392216444', '1392981305', '1', '呵呵，暂时没有内容哦！！', '0');
INSERT INTO `ocenter_config` VALUES ('131', 'USER_RESPASS', '5', '密码找回模板', '3', '', '密码找回文本', '1396191234', '1396191234', '1', '<span style=\"color:#009900;\">请点击以下链接找回密码，如无反应，请将链接地址复制到浏览器中打开(下次登录前有效)</span>', '0');
INSERT INTO `ocenter_config` VALUES ('132', 'COUNT_CODE', '2', '统计代码', '1', '', '用于统计网站访问量的第三方代码，推荐CNZZ统计', '1403058890', '1403058890', '1', '', '12');
INSERT INTO `ocenter_config` VALUES ('134', 'URL_MODEL', '4', 'URL模式', '4', '2:REWRITE模式(开启伪静态)\r\n3:兼容模式', '选择Rewrite模式则开启伪静态，在开启伪静态之前需要先<a href=\"http://v2.opensns.cn/index.php?s=/news/index/detail/id/128.html\" target=\"_blank\">设置伪静态</a>或者阅读/Rewrite/readme.txt中的说明，默认建议开启兼容模式', '1421027546', '1421027676', '1', '3', '0');
INSERT INTO `ocenter_config` VALUES ('135', 'DEFUALT_HOME_URL', '1', '登录前首页Url', '1', '', '支持形如weibo/index/index的ThinkPhp路由写法，支持普通的url写法，不填则显示默认聚合首页', '1417509438', '1427340006', '1', '', '1');
INSERT INTO `ocenter_config` VALUES ('136', 'AUTO_UPDATE', '4', '自动更新提示', '1', '0:关闭,1:开启', '关闭后，后台将不显示更新提示', '1433731153', '1433731348', '1', '1', '2');
INSERT INTO `ocenter_config` VALUES ('137', 'WEB_SITE_CLOSE_HINT', '2', '关站提示文字', '1', '', '站点关闭后的提示文字。', '1433731248', '1433731287', '1', '网站正在更新维护，请稍候再试。', '4');
INSERT INTO `ocenter_config` VALUES ('138', 'SESSION_PREFIX', '1', '网站前台session前缀', '1', '', '当多个网站在同一个根域名下请保证每个网站的前缀不同', '1436923664', '1436923664', '1', 'opensns', '20');
INSERT INTO `ocenter_config` VALUES ('139', 'COOKIE_PREFIX', '1', '网站前台cookie前缀', '1', '', '当多个网站在同一个根域名下请保证每个网站的前缀不同', '1436923664', '1436923664', '1', 'opensns_', '21');
INSERT INTO `ocenter_config` VALUES ('141', 'LOST_LONG', '0', '用户流失标准（天）', '0', '', '', '1469414315', '1469414315', '1', '30', '0');
INSERT INTO `ocenter_config` VALUES ('142', 'MAIL_VERIFY_TYPE', '4', 'SMTP验证方式', '5', '1:无\r\n2:ssl\r\n3:tls', 'SMTP验证方式', '1388332882', '1388931416', '1', '1', '0');
INSERT INTO `ocenter_config` VALUES ('145', '_USERCONFIG_REG_SWITCH', '0', '', '0', '', '', '1427094903', '1427094903', '1', 'email', '0');
INSERT INTO `ocenter_config` VALUES ('146', '_USERCONFIG_UCENTER_KANBAN', '0', '', '0', '', '', '1501048026', '1501048026', '1', '[{\"data-id\":\"disable\",\"title\":\"禁用\",\"items\":[{\"id\":\"News\",\"title\":\"资讯\",\"data-id\":\"News\"}]},{\"data-id\":\"enable\",\"title\":\"启用\",\"items\":[{\"id\":\"Weibo\",\"title\":\"微博\",\"data-id\":\"Weibo\"},{\"id\":\"follow\",\"title\":\"TA的关注/粉丝\",\"data-id\":\"follow\"},{\"id\":\"info\",\"title\":\"资料\",\"data-id\":\"info\"},{\"id\":\"rank_title\",\"title\":\"头衔\",\"data-id\":\"rank_title\"},{\"id\":\"topic_list\",\"title\":\"关注的话题\",\"data-id\":\"topic_list\"}]}]', '0');
INSERT INTO `ocenter_config` VALUES ('152', '_CONFIG_WEB_SITE_NAME', '0', '', '0', '', '', '1427339647', '1427339647', '1', '教学管理系统V6', '0');
INSERT INTO `ocenter_config` VALUES ('153', '_CONFIG_ICP', '0', '', '0', '', '', '1427339647', '1427339647', '1', '浙ICP备12042711号-5', '0');
INSERT INTO `ocenter_config` VALUES ('154', '_CONFIG_LOGO', '0', '', '0', '', '', '1427339647', '1427339647', '1', '', '0');
INSERT INTO `ocenter_config` VALUES ('155', '_CONFIG_QRCODE', '0', '', '0', '', '', '1427339647', '1427339647', '1', '', '0');
INSERT INTO `ocenter_config` VALUES ('156', '_CONFIG_ABOUT_US', '0', '', '0', '', '', '1427339647', '1427339647', '1', '<p>&nbsp; &nbsp;天台县外国语学校坐落于飞鹤山麓，校园倚山傍湖，绿树成荫，鲜花溢香，自然风光旖旎，环境幽雅宁静。</p>', '0');
INSERT INTO `ocenter_config` VALUES ('157', '_CONFIG_SUBSCRIB_US', '0', '', '0', '', '', '1427339647', '1427339647', '1', '<p>联系地址：浙江省天台县赤城街道育才路1号</p>', '0');
INSERT INTO `ocenter_config` VALUES ('158', '_CONFIG_COPY_RIGHT', '0', '', '0', '', '', '1427339647', '1427339647', '1', '<p>Copyright ©2014-2019 <a href=\"http://www.zjttwgy.com\" target=\"_blank\">天台县外国语学校</a></p>', '0');
INSERT INTO `ocenter_config` VALUES ('159', '_CONFIG_FIRST_USER_RUN', '0', '', '0', '', '', '1553775653', '1553775653', '1', '2019-03-28', '0');
INSERT INTO `ocenter_config` VALUES ('160', '_CONFIG_WEBSOCKET_PORT', '0', 'webscoket端口号', '4', '1:80,2:8000', '系统默认应该为：8000\r\n尝试用80', '1553779826', '1553779861', '1', '80', '3');
INSERT INTO `ocenter_config` VALUES ('161', '_CONFIG_WEBSOCKET_ADDRESS', '1', 'WEBSOCKET_ADDRESS', '4', '1:127.0.0.1\r\n2：localhost\r\n', '', '1553781669', '1553781791', '1', '127.0.0.1', '2');
