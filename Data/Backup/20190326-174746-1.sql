-- -----------------------------
-- Think MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 3306
-- Database : teaching_management
-- 
-- Part : #1
-- Date : 2019-03-26 17:47:46
-- -----------------------------


DROP TABLE IF EXISTS `ocenter_super_links`;
CREATE TABLE `ocenter_super_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类别（1：图片，2：普通）',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '站点名称',
  `cover_id` int(10) NOT NULL COMMENT '图片ID',
  `link` char(140) NOT NULL DEFAULT '' COMMENT '链接地址',
  `level` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '优先级',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态（0：禁用，1：正常）',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='友情连接表' AUTO_INCREMENT=8 ;


INSERT INTO `ocenter_super_links` (`id`, `type`, `title`, `cover_id`, `link`, `level`, `status`, `create_time`) VALUES
(1, '2', 'ZUI', '0', 'http://zui.sexy', '0', 1, '1539705029'),
(2, '2', '百度', '0', 'http://www.baidu.com/', '0', 1, '1539705134'),
(3, '2', 'thinkphp', '0', 'http://www.thinkphp.cn', '0', '1', '1553496828'),
(4, '2', '码云', '0', 'https://gitee.com/shuiyueju8/', '0', '1', '1553497030'),
(5, '2', 'php笔记', '0', 'http://10.110.0.14/mynotes/', '0', '1', '1553497193'),
(6, '2', '水月居的博客', '0', 'http://blog.sina.com.cn/shuiyueju8', '0', '1', '1553498201'),
(7, '2', '论文检测(paperpass)', '0', 'http://www.paperpass.com/f/eg0nh9', '0', '1', '1553498719');