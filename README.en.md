# Shuiyueju Teaching Management Platform [教学管理平台]

#### Description
Developing school management platform based on opensns6 can be used in school teaching management, integrating teacher award-winning registration, student award-winning registration, student achievement management statistics (in development), [student management, subject management, lesson preparation system, colorful courses]
(module in brackets is not open).

#### Software Architecture

Software architecture description: ZUI1.8.1 + THINKPHP 3.2.3 + JQUERY + PHPexcel 1.8 + PHPword 1.2


#### Installation

1. start setup input http://127.0.0.1/teaching_management/ to start setup
2. set term set year
3. read help

#### Instructions

1. Upgrade Zui 1.2 to 1.81 in the original opensns6
2. Add PHPexcel module to support database export/import excel documents, modify the style to support document style design [table line, header font, header header style bold].
3. Fix many BUGs in the code, modify when the project file is not in the root directory at the bottom of the Friendship Link picture does not show the problem.


#### Contribution

1. Fork the repository
2. star the repository
3. Commit your code [ git add . ] 、[ git commmit -m "infomation" ] 、[ git push ]
4. Create Pull Request [ git fetch ] 、[ git pull ]
5. Clone the repository to lacltion [ git clone https://gitee.com/shuiyueju/teaching_management.git ]


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)