<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------


namespace Wckc\Widget;

use Think\Controller;

/**
 * 分类widget
 * 用于动态调用分类信息
 */
class MenuWidget extends Controller
{

    /* 显示左侧分类列表 */
    public function showSidebar($id = '', $title = '标题',$width='100%',$height='200px',$config='')
    {

           $this->assign('id',$id);
           $this->assign('title',$title );

        // $this->assign('width',$width);
        // $this->assign('height',$height);
        // $this->assign('config',$config);
        $this->display(T('Application://Wckc@Widget/sidebar'));
        
    }

}
