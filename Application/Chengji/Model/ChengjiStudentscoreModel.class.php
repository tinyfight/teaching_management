<?php
namespace Chengji\Model;
use Think\Model;

/**
 * 学生成绩汇总模型
 * @author 水月居 <singliang@163.com>
 */

class ChengjiStudentScoreModel extends Model {

    protected $_validate = array(
        array('st_name', '1,6', '姓名长度为1-6个字符', self::EXISTS_VALIDATE, 'length'),
        //array('class', '4,8', '班级名称长度为4-8个字符', self::EXISTS_VALIDATE, 'length'),
       
    );

    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
        array('uid', 'is_login',3, 'function'),
               
    );


    public function getData($id){
        return $this->find($id);
    }


    public function getListByPage($map,$page=1,$order='sort asc,update_time desc', $field='*',$r=30)
    {
        $totalCount=$this->where($map)->count();
        if($totalCount){
            $list=$this->relation(true)->where($map)->page($page,$r)->order($order)->select();
        }
        return array($list,$totalCount,$r);
    }

}

