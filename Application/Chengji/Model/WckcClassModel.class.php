<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-5-28
 * Time: 下午3:11
 * @author 水月居<singliang@163.com>
 */

namespace Wckc\Model;
use Think\Model;

class WckcClassModel extends Model{

     protected $_validate = array(
          array('bj_name', '1,20', '班级名称长度不合法', self::EXISTS_VALIDATE, 'length'),
          array('bj_name', '', '该班级名称已经存在', self::EXISTS_VALIDATE, 'unique'), //班级名称占用          
          array('bj_code', '6', '班级代号长度必须为6位', self::EXISTS_VALIDATE, 'length'), //班级代号重复          
        
    );
        protected $_auto = array(
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
        
    );
} 