<?php
namespace Wckc\Model;
use Think\Model\RelationModel;

/**
 * 学生模型
 * @author 水月居 <singliang@163.com>
 */

class WckcFilesModel extends RelationModel {
//Relation
    protected $_validate = array(
        array('teacher', '1,10', '教师姓名长度为1-10个字符', self::EXISTS_VALIDATE, 'length'),
        
    );

    protected $_auto = array(
        // array('update_time', NOW_TIME, self::MODEL_BOTH),
        // array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
        array('uid', 'is_login',3, 'function'),
        
    );

    protected $_link = array(
        'Wckc_course'=>array(
             'mapping_type'=> self::BELONGS_TO,
             'class_name' => 'Wckc_course',
		     'mapping_fields'=>'cname,teacher',
		     //'mapping_name'  => 'cname',
             'foreign_key' => 'cid',
             'as_fields'=>'cname,teacher',
		),
   
    );

    public function getName($sid){
        return $this->where(array('uid'=>(int)$sid))->getField('name');
    }

    /**
     * 检测当前上传的文件是否已经存在
     * @param  array   $file 文件上传数组
     * @return boolean       文件信息， false - 不存在该文件
     */
    public function isFile($file){
        if(empty($file['md5'])){
            throw new Exception('缺少参数:md5');
        }
        /* 查找文件 */
        $map = array('md5' => $file['md5']);
        return $this->field(true)->where($map)->find();
    }
    
    /**
     * 下载本地文件
     * @param  array    $file     文件信息数组：file_url文件地址；file_name下载的文件名；
     * @return boolean            下载失败返回false
     */
    public function downLocalFile($file_url,$file_name,$file_type){
        $fileurl=iconv("UTF-8","gb2312",$file_url);
        if(is_file($fileurl)){ 
               if(!$file_type){
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $file_type = finfo_file($finfo, $fileurl);//获取文件类型
                $file_type = $file_type? $file_type : 'application/msword';//设置默认为word                
                }     
    

                $file['size'] = filesize($fileurl); //获取文件大小
             // dump($file_name);die;    
            
            /* 执行下载 */ 
             
            header("Content-Description: File Transfer");
         
            header('Content-type: ' . $file_type);
            header('Content-Length:' . $file['size']);
           /*兼容IE文件名中文设置*/
            if (is_ie()) {
                $encoded_filename = rawurlencode($file_name);  
                header('Content-Disposition: attachment; filename="'. $encoded_filename . '"');             
            } else {
                header('Content-Disposition: attachment; filename="' . $file_name . '"');
            }
           
            readfile($fileurl);
            exit;
        } else {
            $this->error = '文件已被删除！';
            return false;
        }
    }
   /**@todo
  *下载附件*
  **/
   public function download(){
    $id=I('fid');
    $afile=$this->fileModel->find($id);

    //判断文件是否存在
    $file_path = C('_ROOTPATH').$afile['file_url'];
    
    $file_path = iconv('utf-8', 'gb2312', $file_path); //对可能出现的中文名称进行转码
    if (!file_exists($file_path)) {
      $this->error('文件不存在！');
    }
    $file_name =$afile['file_name']; //获取文件名称 basename()
    $file_size = filesize($file_path); //获取文件大小
    $fp = fopen($file_path, 'r'); //以只读的方式打开文件
    header("Content-type: application/octet-stream");
    header("Accept-Ranges: bytes");
    header("Accept-Length: {$file_size}");
                 if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
                 header('Content-Disposition: attachment; filename="' . rawurlencode($file_name) . '"');
             } else {
                 header('Content-Disposition: attachment; filename="' . $file_name . '"');
             }
    //header("Content-Disposition: attachment;filename={$file_name}");
    $buffer = 1024;
    $file_count = 0;
    //判断文件是否结束
    while (!feof($fp) && ($file_size-$file_count>0)) {
    $file_data = fread($fp, $buffer);
    $file_count += $buffer;
    echo $file_data;
    }
    fclose($fp); //关闭文件
    }






    public function getListByPage($map,$page=1,$order='sort asc,update_time desc',$field='*',$r=20){
        $totalCount=$this->where($map)->count();
        if($totalCount){
            $list=$this->where($map)->page($page,$r)->order($order)->field($field)->select();
        }
        return array($list,$totalCount,$r);
    }

}
