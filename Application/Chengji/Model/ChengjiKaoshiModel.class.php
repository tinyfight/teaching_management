<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-5-28
 * Time: 下午3:11
 * @author 水月居<singliang@163.com>
 */

namespace Chengji\Model;
use Think\Model;

class ChengjiKaoshiModel extends Model{

    protected $_validate = array(
        array('name', 'require', '标识不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', '', '标识已经存在', self::VALUE_VALIDATE, 'unique', self::MODEL_BOTH),
        array('name_s', 'require', '名称不能为空', self::MUST_VALIDATE , 'unique', self::MODEL_BOTH),
    );
    protected $_auto = array(
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
        array('uid', 'is_login',3, 'function'),
    );

     /**
     * 更新考试信息
     * @return boolean 更新状态
     * @author 水月居 <singliang@qq.com>
     */
    public function update(){
        $data = $this->create();
        if(!$data){ //数据对象创建错误
            return false;
        }

        /* 添加或更新数据 */
        return empty($data['id']) ? $this->add() : $this->save();
    }
     /**
     * 删除考试信息
     * @return boolean 删除状态
     * @author 水月居 <singliang@qq.com>
     */
    public function deleteKaoshi($data){
        if(!$data){ //数据对象创建错误
            return false;
        }
        if(!is_array($data)){
          $data = str2arr($data);
        }
        $del_count = 0;
        foreach($data as $id){
          $this->delete($id);
          $del_count++;
        }  
        return  '成功删除'.$del_count.'条考试信息';
    }
} 
