<?php
namespace Wckc\Model;
use Think\Model\RelationModel;

/**
 * 学生模型
 * @author 水月居 <singliang@163.com>
 */

class WckcJiemuModel extends RelationModel {
//Relation
    protected $_validate = array(
        array('name', '1,10', '姓名长度为1-10个字符', self::EXISTS_VALIDATE, 'length'),
        array('name', '', '姓名已经被占用', self::EXISTS_VALIDATE, 'unique'), //用户名被占用
    );
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('upfile_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT),
    );

    protected $_link = array(
        'Wckc_course'=>array(
             'mapping_type'=> self::BELONGS_TO,
             'class_name' => 'Wckc_course',
		     'mapping_fields'=>'cname,teacher',
		     //'mapping_name'  => 'cname',
             'foreign_key' => 'cid',
             'as_fields'=>'cname,teacher',
		),
   
    );

    public function getName($sid){
        return $this->where(array('uid'=>(int)$sid))->getField('name');
    }
    /**
     * 下载本地文件
     * @param  array    $file     文件信息数组：file_url文件地址；file_name下载的文件名；
     * @return boolean            下载失败返回false
     */
    public function downLocalFile($file_url,$file_name,$file_type){
        $fileurl=iconv("UTF-8","gb2312",$file_url);
        if(is_file($fileurl)){
            $file['size'] = filesize($fileurl); //获取文件大小
            /*兼容IE文件名中文设置*/
            $encoded_filename = rawurlencode($file_name);
            //$encoded_filename = str_replace("+", "%20", $encoded_filename);
            
            /* 执行下载 */ 
            $ua = $_SERVER["HTTP_USER_AGENT"]; 
            header("Content-Description: File Transfer");
             
            header('Content-type: ' . $file_type);
            header('Content-Length:' . $file['size']);
            if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
                //header('Content-Disposition: attachment; filename ="' . $encoded_filename . '"');
                header('Content-Disposition: attachment; filename ="' . rawurlencode($file_name) . '"');
            } else {
                header('Content-Disposition: attachment; filename="' . $file_name . '"');
            }
            readfile($fileurl);
            exit;
        } else {
            $this->error = '文件已被删除！';
            return false;
        }
    }
}
