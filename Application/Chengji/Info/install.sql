-- -----------------------------
-- 表结构 `ocenter_chengji_class_score`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_chengji_class_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kid` int(11) DEFAULT NULL,
  `term` char(6) DEFAULT NULL,
  `bj_code` char(8) DEFAULT NULL,
  `bj_name` char(9) DEFAULT NULL,
  `subject` char(8) DEFAULT NULL,
  `teacher` char(10) DEFAULT NULL,
  `pinjunfen` float(6,2) DEFAULT NULL COMMENT '平均分',
  `youxiulv` float(6,3) DEFAULT NULL COMMENT '优秀率',
  `hegelv` float(6,3) DEFAULT NULL COMMENT '合格率',
  `hou10renshu` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=525 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_chengji_grade`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_chengji_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kid` int(11) NOT NULL,
  `grade` int(4) DEFAULT NULL,
  `grade_name` char(9) DEFAULT NULL,
  `subjects` char(40) DEFAULT NULL COMMENT '科目',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_chengji_kaoshi`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_chengji_kaoshi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `term` char(10) DEFAULT NULL,
  `year` mediumint(4) DEFAULT NULL,
  `name_s` char(12) DEFAULT NULL,
  `name` char(30) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `subject` char(200) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_chengji_student`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_chengji_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `term` char(6) DEFAULT NULL,
  `bj_code` int(8) DEFAULT NULL,
  `bj_name` char(20) DEFAULT NULL,
  `st_code` int(8) DEFAULT NULL,
  `st_name` char(8) DEFAULT NULL,
  `sex` char(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1148 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_chengji_student_score`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_chengji_student_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kid` int(11) DEFAULT NULL COMMENT '考试id',
  `term` char(10) DEFAULT NULL COMMENT '学期',
  `grade` tinyint(4) DEFAULT NULL COMMENT '年级',
  `bj_code` mediumint(9) DEFAULT NULL COMMENT '班级编号',
  `bj_name` char(10) DEFAULT NULL COMMENT '班级',
  `st_code` int(11) DEFAULT NULL,
  `st_name` char(10) DEFAULT NULL COMMENT '学生姓名',
  `sex` char(2) DEFAULT NULL COMMENT '性别',
  `subject_id` int(11) DEFAULT NULL,
  `subject` char(12) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `class_order` int(11) DEFAULT NULL,
  `grade_order` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7294 DEFAULT CHARSET=utf8;


-- -----------------------------
-- 表结构 `ocenter_chengji_subject`
-- -----------------------------
CREATE TABLE IF NOT EXISTS `ocenter_chengji_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` char(20) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- -----------------------------
-- 表内记录 `ocenter_chengji_grade`
-- -----------------------------
INSERT INTO `ocenter_chengji_grade` VALUES ('1', '4', '1', '1年级', '语文,数学,英语');
INSERT INTO `ocenter_chengji_grade` VALUES ('2', '4', '2', '2年级', '语文数学,英语');
INSERT INTO `ocenter_chengji_grade` VALUES ('3', '4', '3', '3年级', '语文,数学,科学,英语,英语（口语）,英语（笔试）');
INSERT INTO `ocenter_chengji_grade` VALUES ('4', '4', '4', '4年级', '语文,数学,科学,英语,英语（口语）,英语（笔试）');
INSERT INTO `ocenter_chengji_grade` VALUES ('5', '4', '5', '5年级', '语文,数学,科学,英语,英语（口语）,英语（笔试）');
INSERT INTO `ocenter_chengji_grade` VALUES ('6', '4', '6', '6年级', '语文,数学,科学,英语,英语（口语）,英语（笔试）');
-- -----------------------------
-- 表内记录 `ocenter_chengji_kaoshi`
-- -----------------------------
INSERT INTO `ocenter_chengji_kaoshi` VALUES ('1', '2016A', '2016', '2016上', '2016学年第一学期期末考试', '1547282443', '1547282443', '语文,数学,科学,英语', '1', '1');
INSERT INTO `ocenter_chengji_kaoshi` VALUES ('2', '2016B', '2016', '2016下', '2016学年第二学期期末考试', '1547282443', '1547282443', '语文,数学,科学,英语', '1', '1');
INSERT INTO `ocenter_chengji_kaoshi` VALUES ('3', '2017A', '2017', '2017上', '2017学年第一学期期末考试', '1547282443', '1547282443', '语文,数学,科学,英语', '1', '1');
INSERT INTO `ocenter_chengji_kaoshi` VALUES ('4', '2017B', '2017', '2017下', '2017学年第二学期期末考试', '1547282443', '1547282443', '语文,数学,科学,英语', '1', '1');
INSERT INTO `ocenter_chengji_kaoshi` VALUES ('5', '2018A', '2018', '2018上', '2018学年第一学期期末考试', '1547282443', '1547282443', '语文,数学,科学,英语', '1', '1');
INSERT INTO `ocenter_chengji_kaoshi` VALUES ('6', '2018A', '2018', '知识竞赛', '知识竞赛', '1547282443', '1547282443', '', '', '1');
-- -----------------------------
-- 表内记录 `ocenter_chengji_student`
-- -----------------------------
INSERT INTO `ocenter_chengji_student` VALUES ('918', '2017B', '201801', '一年级1班', '1', '丁希灿', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('919', '2017B', '201801', '一年级1班', '2', '王子恒', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('920', '2017B', '201801', '一年级1班', '3', '王哲烜', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('921', '2017B', '201801', '一年级1班', '4', '许雨欣', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('922', '2017B', '201801', '一年级1班', '5', '许雨菲', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('923', '2017B', '201801', '一年级1班', '6', '许庞羽', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('924', '2017B', '201801', '一年级1班', '7', '许家豪', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('925', '2017B', '201801', '一年级1班', '8', '许梓童', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('926', '2017B', '201801', '一年级1班', '9', '杨竣皓', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('927', '2017B', '201801', '一年级1班', '10', '张可馨', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('928', '2017B', '201801', '一年级1班', '11', '陈泽楷', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('929', '2017B', '201801', '一年级1班', '12', '陈奕帆', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('930', '2017B', '201801', '一年级1班', '13', '陈嘉琪', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('931', '2017B', '201801', '一年级1班', '14', '陈潇廷', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('932', '2017B', '201801', '一年级1班', '15', '范可心', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('933', '2017B', '201801', '一年级1班', '16', '范莎莎', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('934', '2017B', '201801', '一年级1班', '17', '林凯波', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('935', '2017B', '201801', '一年级1班', '18', '郑尧瑛', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('936', '2017B', '201801', '一年级1班', '19', '郑梦涵', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('937', '2017B', '201801', '一年级1班', '20', '郑博瀚', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('938', '2017B', '201801', '一年级1班', '21', '杨英杰', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('939', '2017B', '201801', '一年级1班', '22', '杨祥豪', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('940', '2017B', '201801', '一年级1班', '23', '汪子恒', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('941', '2017B', '201801', '一年级1班', '24', '金天浩', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('942', '2017B', '201801', '一年级1班', '25', '庞语涵', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('943', '2017B', '201801', '一年级1班', '26', '郑能杰', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('944', '2017B', '201801', '一年级1班', '27', '徐陈轩', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('945', '2017B', '201801', '一年级1班', '28', '徐佳怡', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('946', '2017B', '201801', '一年级1班', '29', '葛致远', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('947', '2017B', '201801', '一年级1班', '30', '景帝翔', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('948', '2017B', '201801', '一年级1班', '31', '蔡雨憧', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('949', '2017B', '201801', '一年级1班', '32', '裴轩恺', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('950', '2017B', '201802', '一年级2班', '1', '王自立', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('951', '2017B', '201802', '一年级2班', '2', '王欣悦', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('952', '2017B', '201802', '一年级2班', '3', '许宇帆', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('953', '2017B', '201802', '一年级2班', '4', '许溢嘉', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('954', '2017B', '201802', '一年级2班', '5', '杨坤涛', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('955', '2017B', '201802', '一年级2班', '6', '王俊熙', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('956', '2017B', '201802', '一年级2班', '7', '张家辉', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('957', '2017B', '201802', '一年级2班', '8', '陈书晗', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('958', '2017B', '201802', '一年级2班', '9', '陈若菡', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('959', '2017B', '201802', '一年级2班', '10', '陈昱帆', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('960', '2017B', '201802', '一年级2班', '11', '陈思淼', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('961', '2017B', '201802', '一年级2班', '12', '陈思璇', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('962', '2017B', '201802', '一年级2班', '13', '陈熙媛', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('963', '2017B', '201802', '一年级2班', '14', '林子涵', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('964', '2017B', '201802', '一年级2班', '15', '金城仡', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('965', '2017B', '201802', '一年级2班', '16', '金晨曦', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('966', '2017B', '201802', '一年级2班', '17', '郑尚毅', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('967', '2017B', '201802', '一年级2班', '18', '汤雅婷', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('968', '2017B', '201802', '一年级2班', '19', '李炫洁', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('969', '2017B', '201802', '一年级2班', '20', '杨子安', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('970', '2017B', '201802', '一年级2班', '21', '邱福睿', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('971', '2017B', '201802', '一年级2班', '22', '余耀文', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('972', '2017B', '201802', '一年级2班', '23', '汪欣怡', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('973', '2017B', '201802', '一年级2班', '24', '张珺涵', '女');
INSERT INTO `ocenter_chengji_student` VALUES ('974', '2017B', '201802', '一年级2班', '25', '姜彦好', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('975', '2017B', '201802', '一年级2班', '26', '谢星罗', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('976', '2017B', '201802', '一年级2班', '27', '鲍宇轩', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('977', '2017B', '201802', '一年级2班', '28', '庞景燦', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('978', '2017B', '201802', '一年级2班', '29', '潘嘉轩', '男');
INSERT INTO `ocenter_chengji_student` VALUES ('979', '2017B', '201802', '一年级2班', '30', '戴山林', '男');

-- -----------------------------
-- 表内记录 `ocenter_chengji_student_score`
-- -----------------------------
INSERT INTO `ocenter_chengji_student_score` VALUES ('1', '4', '2017B', '1', '201801', '一年级1班', '1', '丁希灿', '男', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('2', '4', '2017B', '1', '201801', '一年级1班', '1', '丁希灿', '男', '', '数学', '87', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('3', '4', '2017B', '1', '201801', '一年级1班', '1', '丁希灿', '男', '', '英语', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('4', '4', '2017B', '1', '201801', '一年级1班', '1', '丁希灿', '男', '', '总分', '274', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('5', '4', '2017B', '1', '201801', '一年级1班', '2', '王子恒', '男', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('6', '4', '2017B', '1', '201801', '一年级1班', '2', '王子恒', '男', '', '数学', '87', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('7', '4', '2017B', '1', '201801', '一年级1班', '2', '王子恒', '男', '', '英语', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('8', '4', '2017B', '1', '201801', '一年级1班', '2', '王子恒', '男', '', '总分', '273', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('9', '4', '2017B', '1', '201801', '一年级1班', '3', '王哲烜', '男', '', '语文', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('10', '4', '2017B', '1', '201801', '一年级1班', '3', '王哲烜', '男', '', '数学', '84', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('11', '4', '2017B', '1', '201801', '一年级1班', '3', '王哲烜', '男', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('12', '4', '2017B', '1', '201801', '一年级1班', '3', '王哲烜', '男', '', '总分', '277', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('13', '4', '2017B', '1', '201801', '一年级1班', '4', '许雨欣', '女', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('14', '4', '2017B', '1', '201801', '一年级1班', '4', '许雨欣', '女', '', '数学', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('15', '4', '2017B', '1', '201801', '一年级1班', '4', '许雨欣', '女', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('16', '4', '2017B', '1', '201801', '一年级1班', '4', '许雨欣', '女', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('17', '4', '2017B', '1', '201801', '一年级1班', '5', '许雨菲', '女', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('18', '4', '2017B', '1', '201801', '一年级1班', '5', '许雨菲', '女', '', '数学', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('19', '4', '2017B', '1', '201801', '一年级1班', '5', '许雨菲', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('20', '4', '2017B', '1', '201801', '一年级1班', '5', '许雨菲', '女', '', '总分', '295', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('21', '4', '2017B', '1', '201801', '一年级1班', '6', '许庞羽', '男', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('22', '4', '2017B', '1', '201801', '一年级1班', '6', '许庞羽', '男', '', '数学', '83', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('23', '4', '2017B', '1', '201801', '一年级1班', '6', '许庞羽', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('24', '4', '2017B', '1', '201801', '一年级1班', '6', '许庞羽', '男', '', '总分', '280', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('25', '4', '2017B', '1', '201801', '一年级1班', '7', '许家豪', '男', '', '语文', '84', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('26', '4', '2017B', '1', '201801', '一年级1班', '7', '许家豪', '男', '', '数学', '85', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('27', '4', '2017B', '1', '201801', '一年级1班', '7', '许家豪', '男', '', '英语', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('28', '4', '2017B', '1', '201801', '一年级1班', '7', '许家豪', '男', '', '总分', '266', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('29', '4', '2017B', '1', '201801', '一年级1班', '8', '许梓童', '女', '', '语文', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('30', '4', '2017B', '1', '201801', '一年级1班', '8', '许梓童', '女', '', '数学', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('31', '4', '2017B', '1', '201801', '一年级1班', '8', '许梓童', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('32', '4', '2017B', '1', '201801', '一年级1班', '8', '许梓童', '女', '', '总分', '289', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('33', '4', '2017B', '1', '201801', '一年级1班', '9', '杨竣皓', '男', '', '语文', '90', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('34', '4', '2017B', '1', '201801', '一年级1班', '9', '杨竣皓', '男', '', '数学', '74', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('35', '4', '2017B', '1', '201801', '一年级1班', '9', '杨竣皓', '男', '', '英语', '86', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('36', '4', '2017B', '1', '201801', '一年级1班', '9', '杨竣皓', '男', '', '总分', '250', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('37', '4', '2017B', '1', '201801', '一年级1班', '10', '张可馨', '女', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('38', '4', '2017B', '1', '201801', '一年级1班', '10', '张可馨', '女', '', '数学', '79', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('39', '4', '2017B', '1', '201801', '一年级1班', '10', '张可馨', '女', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('40', '4', '2017B', '1', '201801', '一年级1班', '10', '张可馨', '女', '', '总分', '274', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('41', '4', '2017B', '1', '201801', '一年级1班', '11', '陈泽楷', '男', '', '语文', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('42', '4', '2017B', '1', '201801', '一年级1班', '11', '陈泽楷', '男', '', '数学', '83', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('43', '4', '2017B', '1', '201801', '一年级1班', '11', '陈泽楷', '男', '', '英语', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('44', '4', '2017B', '1', '201801', '一年级1班', '11', '陈泽楷', '男', '', '总分', '276', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('45', '4', '2017B', '1', '201801', '一年级1班', '12', '陈奕帆', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('46', '4', '2017B', '1', '201801', '一年级1班', '12', '陈奕帆', '男', '', '数学', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('47', '4', '2017B', '1', '201801', '一年级1班', '12', '陈奕帆', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('48', '4', '2017B', '1', '201801', '一年级1班', '12', '陈奕帆', '男', '', '总分', '293', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('49', '4', '2017B', '1', '201801', '一年级1班', '13', '陈嘉琪', '女', '', '语文', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('50', '4', '2017B', '1', '201801', '一年级1班', '13', '陈嘉琪', '女', '', '数学', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('51', '4', '2017B', '1', '201801', '一年级1班', '13', '陈嘉琪', '女', '', '英语', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('52', '4', '2017B', '1', '201801', '一年级1班', '13', '陈嘉琪', '女', '', '总分', '290', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('53', '4', '2017B', '1', '201801', '一年级1班', '14', '陈潇廷', '男', '', '语文', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('54', '4', '2017B', '1', '201801', '一年级1班', '14', '陈潇廷', '男', '', '数学', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('55', '4', '2017B', '1', '201801', '一年级1班', '14', '陈潇廷', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('56', '4', '2017B', '1', '201801', '一年级1班', '14', '陈潇廷', '男', '', '总分', '287', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('57', '4', '2017B', '1', '201801', '一年级1班', '15', '范可心', '女', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('58', '4', '2017B', '1', '201801', '一年级1班', '15', '范可心', '女', '', '数学', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('59', '4', '2017B', '1', '201801', '一年级1班', '15', '范可心', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('60', '4', '2017B', '1', '201801', '一年级1班', '15', '范可心', '女', '', '总分', '287', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('61', '4', '2017B', '1', '201801', '一年级1班', '16', '范莎莎', '女', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('62', '4', '2017B', '1', '201801', '一年级1班', '16', '范莎莎', '女', '', '数学', '87', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('63', '4', '2017B', '1', '201801', '一年级1班', '16', '范莎莎', '女', '', '英语', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('64', '4', '2017B', '1', '201801', '一年级1班', '16', '范莎莎', '女', '', '总分', '276', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('65', '4', '2017B', '1', '201801', '一年级1班', '17', '林凯波', '男', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('66', '4', '2017B', '1', '201801', '一年级1班', '17', '林凯波', '男', '', '数学', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('67', '4', '2017B', '1', '201801', '一年级1班', '17', '林凯波', '男', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('68', '4', '2017B', '1', '201801', '一年级1班', '17', '林凯波', '男', '', '总分', '292', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('69', '4', '2017B', '1', '201801', '一年级1班', '18', '郑尧瑛', '女', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('70', '4', '2017B', '1', '201801', '一年级1班', '18', '郑尧瑛', '女', '', '数学', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('71', '4', '2017B', '1', '201801', '一年级1班', '18', '郑尧瑛', '女', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('72', '4', '2017B', '1', '201801', '一年级1班', '18', '郑尧瑛', '女', '', '总分', '284', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('73', '4', '2017B', '1', '201801', '一年级1班', '19', '郑梦涵', '女', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('74', '4', '2017B', '1', '201801', '一年级1班', '19', '郑梦涵', '女', '', '数学', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('75', '4', '2017B', '1', '201801', '一年级1班', '19', '郑梦涵', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('76', '4', '2017B', '1', '201801', '一年级1班', '19', '郑梦涵', '女', '', '总分', '293', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('77', '4', '2017B', '1', '201801', '一年级1班', '20', '郑博瀚', '男', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('78', '4', '2017B', '1', '201801', '一年级1班', '20', '郑博瀚', '男', '', '数学', '84', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('79', '4', '2017B', '1', '201801', '一年级1班', '20', '郑博瀚', '男', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('80', '4', '2017B', '1', '201801', '一年级1班', '20', '郑博瀚', '男', '', '总分', '275', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('81', '4', '2017B', '1', '201801', '一年级1班', '21', '杨英杰', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('82', '4', '2017B', '1', '201801', '一年级1班', '21', '杨英杰', '男', '', '数学', '89', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('83', '4', '2017B', '1', '201801', '一年级1班', '21', '杨英杰', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('84', '4', '2017B', '1', '201801', '一年级1班', '21', '杨英杰', '男', '', '总分', '287', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('85', '4', '2017B', '1', '201801', '一年级1班', '22', '杨祥豪', '男', '', '语文', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('86', '4', '2017B', '1', '201801', '一年级1班', '22', '杨祥豪', '男', '', '数学', '83', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('87', '4', '2017B', '1', '201801', '一年级1班', '22', '杨祥豪', '男', '', '英语', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('88', '4', '2017B', '1', '201801', '一年级1班', '22', '杨祥豪', '男', '', '总分', '273', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('89', '4', '2017B', '1', '201801', '一年级1班', '23', '汪子恒', '男', '', '语文', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('90', '4', '2017B', '1', '201801', '一年级1班', '23', '汪子恒', '男', '', '数学', '92', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('91', '4', '2017B', '1', '201801', '一年级1班', '23', '汪子恒', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('92', '4', '2017B', '1', '201801', '一年级1班', '23', '汪子恒', '男', '', '总分', '287', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('93', '4', '2017B', '1', '201801', '一年级1班', '24', '金天浩', '男', '', '语文', '88', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('94', '4', '2017B', '1', '201801', '一年级1班', '24', '金天浩', '男', '', '数学', '70', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('95', '4', '2017B', '1', '201801', '一年级1班', '24', '金天浩', '男', '', '英语', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('96', '4', '2017B', '1', '201801', '一年级1班', '24', '金天浩', '男', '', '总分', '252', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('97', '4', '2017B', '1', '201801', '一年级1班', '25', '庞语涵', '女', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('98', '4', '2017B', '1', '201801', '一年级1班', '25', '庞语涵', '女', '', '数学', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('99', '4', '2017B', '1', '201801', '一年级1班', '25', '庞语涵', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('100', '4', '2017B', '1', '201801', '一年级1班', '25', '庞语涵', '女', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('101', '4', '2017B', '1', '201801', '一年级1班', '26', '郑能杰', '男', '', '语文', '86', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('102', '4', '2017B', '1', '201801', '一年级1班', '26', '郑能杰', '男', '', '数学', '75', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('103', '4', '2017B', '1', '201801', '一年级1班', '26', '郑能杰', '男', '', '英语', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('104', '4', '2017B', '1', '201801', '一年级1班', '26', '郑能杰', '男', '', '总分', '258', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('105', '4', '2017B', '1', '201801', '一年级1班', '27', '徐陈轩', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('106', '4', '2017B', '1', '201801', '一年级1班', '27', '徐陈轩', '男', '', '数学', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('107', '4', '2017B', '1', '201801', '一年级1班', '27', '徐陈轩', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('108', '4', '2017B', '1', '201801', '一年级1班', '27', '徐陈轩', '男', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('109', '4', '2017B', '1', '201801', '一年级1班', '28', '徐佳怡', '女', '', '语文', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('110', '4', '2017B', '1', '201801', '一年级1班', '28', '徐佳怡', '女', '', '数学', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('111', '4', '2017B', '1', '201801', '一年级1班', '28', '徐佳怡', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('112', '4', '2017B', '1', '201801', '一年级1班', '28', '徐佳怡', '女', '', '总分', '295', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('113', '4', '2017B', '1', '201801', '一年级1班', '29', '葛致远', '男', '', '语文', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('114', '4', '2017B', '1', '201801', '一年级1班', '29', '葛致远', '男', '', '数学', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('115', '4', '2017B', '1', '201801', '一年级1班', '29', '葛致远', '男', '', '英语', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('116', '4', '2017B', '1', '201801', '一年级1班', '29', '葛致远', '男', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('117', '4', '2017B', '1', '201801', '一年级1班', '30', '景帝翔', '男', '', '语文', '92', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('118', '4', '2017B', '1', '201801', '一年级1班', '30', '景帝翔', '男', '', '数学', '89', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('119', '4', '2017B', '1', '201801', '一年级1班', '30', '景帝翔', '男', '', '英语', '90', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('120', '4', '2017B', '1', '201801', '一年级1班', '30', '景帝翔', '男', '', '总分', '271', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('121', '4', '2017B', '1', '201801', '一年级1班', '31', '蔡雨憧', '男', '', '语文', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('122', '4', '2017B', '1', '201801', '一年级1班', '31', '蔡雨憧', '男', '', '数学', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('123', '4', '2017B', '1', '201801', '一年级1班', '31', '蔡雨憧', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('124', '4', '2017B', '1', '201801', '一年级1班', '31', '蔡雨憧', '男', '', '总分', '292', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('125', '4', '2017B', '1', '201801', '一年级1班', '32', '裴轩恺', '男', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('126', '4', '2017B', '1', '201801', '一年级1班', '32', '裴轩恺', '男', '', '数学', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('127', '4', '2017B', '1', '201801', '一年级1班', '32', '裴轩恺', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('128', '4', '2017B', '1', '201801', '一年级1班', '32', '裴轩恺', '男', '', '总分', '290', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('129', '4', '2017B', '1', '201802', '一年级2班', '1', '王自立', '男', '', '语文', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('130', '4', '2017B', '1', '201802', '一年级2班', '1', '王自立', '男', '', '数学', '88', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('131', '4', '2017B', '1', '201802', '一年级2班', '1', '王自立', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('132', '4', '2017B', '1', '201802', '一年级2班', '1', '王自立', '男', '', '总分', '279', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('133', '4', '2017B', '1', '201802', '一年级2班', '2', '王欣悦', '女', '', '语文', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('134', '4', '2017B', '1', '201802', '一年级2班', '2', '王欣悦', '女', '', '数学', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('135', '4', '2017B', '1', '201802', '一年级2班', '2', '王欣悦', '女', '', '英语', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('136', '4', '2017B', '1', '201802', '一年级2班', '2', '王欣悦', '女', '', '总分', '287', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('137', '4', '2017B', '1', '201802', '一年级2班', '3', '许宇帆', '男', '', '语文', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('138', '4', '2017B', '1', '201802', '一年级2班', '3', '许宇帆', '男', '', '数学', '87', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('139', '4', '2017B', '1', '201802', '一年级2班', '3', '许宇帆', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('140', '4', '2017B', '1', '201802', '一年级2班', '3', '许宇帆', '男', '', '总分', '280', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('141', '4', '2017B', '1', '201802', '一年级2班', '4', '许溢嘉', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('142', '4', '2017B', '1', '201802', '一年级2班', '4', '许溢嘉', '男', '', '数学', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('143', '4', '2017B', '1', '201802', '一年级2班', '4', '许溢嘉', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('144', '4', '2017B', '1', '201802', '一年级2班', '4', '许溢嘉', '男', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('145', '4', '2017B', '1', '201802', '一年级2班', '5', '杨坤涛', '男', '', '语文', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('146', '4', '2017B', '1', '201802', '一年级2班', '5', '杨坤涛', '男', '', '数学', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('147', '4', '2017B', '1', '201802', '一年级2班', '5', '杨坤涛', '男', '', '英语', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('148', '4', '2017B', '1', '201802', '一年级2班', '5', '杨坤涛', '男', '', '总分', '290', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('149', '4', '2017B', '1', '201802', '一年级2班', '6', '王俊熙', '男', '', '语文', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('150', '4', '2017B', '1', '201802', '一年级2班', '6', '王俊熙', '男', '', '数学', '93', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('151', '4', '2017B', '1', '201802', '一年级2班', '6', '王俊熙', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('152', '4', '2017B', '1', '201802', '一年级2班', '6', '王俊熙', '男', '', '总分', '284', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('153', '4', '2017B', '1', '201802', '一年级2班', '7', '张家辉', '男', '', '语文', '84', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('154', '4', '2017B', '1', '201802', '一年级2班', '7', '张家辉', '男', '', '数学', '73', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('155', '4', '2017B', '1', '201802', '一年级2班', '7', '张家辉', '男', '', '英语', '80', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('156', '4', '2017B', '1', '201802', '一年级2班', '7', '张家辉', '男', '', '总分', '237', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('157', '4', '2017B', '1', '201802', '一年级2班', '8', '陈书晗', '女', '', '语文', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('158', '4', '2017B', '1', '201802', '一年级2班', '8', '陈书晗', '女', '', '数学', '89', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('159', '4', '2017B', '1', '201802', '一年级2班', '8', '陈书晗', '女', '', '英语', '90', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('160', '4', '2017B', '1', '201802', '一年级2班', '8', '陈书晗', '女', '', '总分', '275', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('161', '4', '2017B', '1', '201802', '一年级2班', '9', '陈若菡', '女', '', '语文', '88', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('162', '4', '2017B', '1', '201802', '一年级2班', '9', '陈若菡', '女', '', '数学', '86', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('163', '4', '2017B', '1', '201802', '一年级2班', '9', '陈若菡', '女', '', '英语', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('164', '4', '2017B', '1', '201802', '一年级2班', '9', '陈若菡', '女', '', '总分', '265', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('165', '4', '2017B', '1', '201802', '一年级2班', '10', '陈昱帆', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('166', '4', '2017B', '1', '201802', '一年级2班', '10', '陈昱帆', '男', '', '数学', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('167', '4', '2017B', '1', '201802', '一年级2班', '10', '陈昱帆', '男', '', '英语', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('168', '4', '2017B', '1', '201802', '一年级2班', '10', '陈昱帆', '男', '', '总分', '292', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('169', '4', '2017B', '1', '201802', '一年级2班', '11', '陈思淼', '男', '', '语文', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('170', '4', '2017B', '1', '201802', '一年级2班', '11', '陈思淼', '男', '', '数学', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('171', '4', '2017B', '1', '201802', '一年级2班', '11', '陈思淼', '男', '', '英语', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('172', '4', '2017B', '1', '201802', '一年级2班', '11', '陈思淼', '男', '', '总分', '293', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('173', '4', '2017B', '1', '201802', '一年级2班', '12', '陈思璇', '女', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('174', '4', '2017B', '1', '201802', '一年级2班', '12', '陈思璇', '女', '', '数学', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('175', '4', '2017B', '1', '201802', '一年级2班', '12', '陈思璇', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('176', '4', '2017B', '1', '201802', '一年级2班', '12', '陈思璇', '女', '', '总分', '293', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('177', '4', '2017B', '1', '201802', '一年级2班', '13', '陈熙媛', '女', '', '语文', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('178', '4', '2017B', '1', '201802', '一年级2班', '13', '陈熙媛', '女', '', '数学', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('179', '4', '2017B', '1', '201802', '一年级2班', '13', '陈熙媛', '女', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('180', '4', '2017B', '1', '201802', '一年级2班', '13', '陈熙媛', '女', '', '总分', '289', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('181', '4', '2017B', '1', '201802', '一年级2班', '14', '林子涵', '男', '', '语文', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('182', '4', '2017B', '1', '201802', '一年级2班', '14', '林子涵', '男', '', '数学', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('183', '4', '2017B', '1', '201802', '一年级2班', '14', '林子涵', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('184', '4', '2017B', '1', '201802', '一年级2班', '14', '林子涵', '男', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('185', '4', '2017B', '1', '201802', '一年级2班', '15', '金城仡', '男', '', '语文', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('186', '4', '2017B', '1', '201802', '一年级2班', '15', '金城仡', '男', '', '数学', '81', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('187', '4', '2017B', '1', '201802', '一年级2班', '15', '金城仡', '男', '', '英语', '85', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('188', '4', '2017B', '1', '201802', '一年级2班', '15', '金城仡', '男', '', '总分', '257', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('189', '4', '2017B', '1', '201802', '一年级2班', '16', '金晨曦', '女', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('190', '4', '2017B', '1', '201802', '一年级2班', '16', '金晨曦', '女', '', '数学', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('191', '4', '2017B', '1', '201802', '一年级2班', '16', '金晨曦', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('192', '4', '2017B', '1', '201802', '一年级2班', '16', '金晨曦', '女', '', '总分', '295', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('193', '4', '2017B', '1', '201802', '一年级2班', '17', '郑尚毅', '男', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('194', '4', '2017B', '1', '201802', '一年级2班', '17', '郑尚毅', '男', '', '数学', '84', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('195', '4', '2017B', '1', '201802', '一年级2班', '17', '郑尚毅', '男', '', '英语', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('196', '4', '2017B', '1', '201802', '一年级2班', '17', '郑尚毅', '男', '', '总分', '276', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('197', '4', '2017B', '1', '201802', '一年级2班', '18', '汤雅婷', '女', '', '语文', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('198', '4', '2017B', '1', '201802', '一年级2班', '18', '汤雅婷', '女', '', '数学', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('199', '4', '2017B', '1', '201802', '一年级2班', '18', '汤雅婷', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('200', '4', '2017B', '1', '201802', '一年级2班', '18', '汤雅婷', '女', '', '总分', '291', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('201', '4', '2017B', '1', '201802', '一年级2班', '19', '李炫洁', '女', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('202', '4', '2017B', '1', '201802', '一年级2班', '19', '李炫洁', '女', '', '数学', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('203', '4', '2017B', '1', '201802', '一年级2班', '19', '李炫洁', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('204', '4', '2017B', '1', '201802', '一年级2班', '19', '李炫洁', '女', '', '总分', '295', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('205', '4', '2017B', '1', '201802', '一年级2班', '20', '杨子安', '男', '', '语文', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('206', '4', '2017B', '1', '201802', '一年级2班', '20', '杨子安', '男', '', '数学', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('207', '4', '2017B', '1', '201802', '一年级2班', '20', '杨子安', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('208', '4', '2017B', '1', '201802', '一年级2班', '20', '杨子安', '男', '', '总分', '289', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('209', '4', '2017B', '1', '201802', '一年级2班', '21', '邱福睿', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('210', '4', '2017B', '1', '201802', '一年级2班', '21', '邱福睿', '男', '', '数学', '94', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('211', '4', '2017B', '1', '201802', '一年级2班', '21', '邱福睿', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('212', '4', '2017B', '1', '201802', '一年级2班', '21', '邱福睿', '男', '', '总分', '292', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('213', '4', '2017B', '1', '201802', '一年级2班', '22', '余耀文', '男', '', '语文', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('214', '4', '2017B', '1', '201802', '一年级2班', '22', '余耀文', '男', '', '数学', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('215', '4', '2017B', '1', '201802', '一年级2班', '22', '余耀文', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('216', '4', '2017B', '1', '201802', '一年级2班', '22', '余耀文', '男', '', '总分', '288', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('217', '4', '2017B', '1', '201802', '一年级2班', '23', '汪欣怡', '女', '', '语文', '99', '', '', '1547380027', '1547544123', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('218', '4', '2017B', '1', '201802', '一年级2班', '23', '汪欣怡', '女', '', '数学', '90', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('219', '4', '2017B', '1', '201802', '一年级2班', '23', '汪欣怡', '女', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('220', '4', '2017B', '1', '201802', '一年级2班', '23', '汪欣怡', '女', '', '总分', '287', '', '', '1547380027', '1547544123', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('221', '4', '2017B', '1', '201802', '一年级2班', '24', '张珺涵', '女', '', '语文', '99', '', '', '1547380027', '1547544123', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('222', '4', '2017B', '1', '201802', '一年级2班', '24', '张珺涵', '女', '', '数学', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('223', '4', '2017B', '1', '201802', '一年级2班', '24', '张珺涵', '女', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('224', '4', '2017B', '1', '201802', '一年级2班', '24', '张珺涵', '女', '', '总分', '299', '', '', '1547380027', '1547544123', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('225', '4', '2017B', '1', '201802', '一年级2班', '25', '姜彦好', '男', '', '语文', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('226', '4', '2017B', '1', '201802', '一年级2班', '25', '姜彦好', '男', '', '数学', '90', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('227', '4', '2017B', '1', '201802', '一年级2班', '25', '姜彦好', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('228', '4', '2017B', '1', '201802', '一年级2班', '25', '姜彦好', '男', '', '总分', '285', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('229', '4', '2017B', '1', '201802', '一年级2班', '26', '谢星罗', '男', '', '语文', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('230', '4', '2017B', '1', '201802', '一年级2班', '26', '谢星罗', '男', '', '数学', '91', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('231', '4', '2017B', '1', '201802', '一年级2班', '26', '谢星罗', '男', '', '英语', '97', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('232', '4', '2017B', '1', '201802', '一年级2班', '26', '谢星罗', '男', '', '总分', '283', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('233', '4', '2017B', '1', '201802', '一年级2班', '27', '鲍宇轩', '男', '', '语文', '96', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('234', '4', '2017B', '1', '201802', '一年级2班', '27', '鲍宇轩', '男', '', '数学', '90', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('235', '4', '2017B', '1', '201802', '一年级2班', '27', '鲍宇轩', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('236', '4', '2017B', '1', '201802', '一年级2班', '27', '鲍宇轩', '男', '', '总分', '286', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('237', '4', '2017B', '1', '201802', '一年级2班', '28', '庞景燦', '男', '', '语文', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('238', '4', '2017B', '1', '201802', '一年级2班', '28', '庞景燦', '男', '', '数学', '95', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('239', '4', '2017B', '1', '201802', '一年级2班', '28', '庞景燦', '男', '', '英语', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('240', '4', '2017B', '1', '201802', '一年级2班', '28', '庞景燦', '男', '', '总分', '292', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('241', '4', '2017B', '1', '201802', '一年级2班', '29', '潘嘉轩', '男', '', '语文', '98', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('242', '4', '2017B', '1', '201802', '一年级2班', '29', '潘嘉轩', '男', '', '数学', '99', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('243', '4', '2017B', '1', '201802', '一年级2班', '29', '潘嘉轩', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('244', '4', '2017B', '1', '201802', '一年级2班', '29', '潘嘉轩', '男', '', '总分', '297', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('245', '4', '2017B', '1', '201802', '一年级2班', '30', '戴山林', '男', '', '语文', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('246', '4', '2017B', '1', '201802', '一年级2班', '30', '戴山林', '男', '', '数学', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('247', '4', '2017B', '1', '201802', '一年级2班', '30', '戴山林', '男', '', '英语', '100', '', '', '1547380027', '1547380027', '1', '1');
INSERT INTO `ocenter_chengji_student_score` VALUES ('248', '4', '2017B', '1', '201802', '一年级2班', '30', '戴山林', '男', '', '总分', '300', '', '', '1547380027', '1547380027', '1', '1');
-- -----------------------------
-- 表内记录 `ocenter_chengji_subject`
-- -----------------------------
INSERT INTO `ocenter_chengji_subject` VALUES ('1', '语文', '1');
INSERT INTO `ocenter_chengji_subject` VALUES ('2', '数学', '2');
INSERT INTO `ocenter_chengji_subject` VALUES ('3', '科学', '3');
INSERT INTO `ocenter_chengji_subject` VALUES ('4', '英语', '6');
INSERT INTO `ocenter_chengji_subject` VALUES ('5', '英语（口语）', '4');
INSERT INTO `ocenter_chengji_subject` VALUES ('6', '英语（笔试）', '5');
