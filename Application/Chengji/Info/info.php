<?php
/**
 * 所属项目 学生成绩统计
 * 开发者: 水月居
 * 创建日期: 2017-12-18
 * 创建时间: 10:14
 * 版权所有 水月居软件工作室(blog.sina.com.cn/shuiyueju8)
 */

return array(
    //模块名
    'name' => 'chengji',
    //别名
    'alias' => '学生成绩统计',
    //版本号
    'version' => '6.0.1',
    //是否商业模块,1是，0，否
    'is_com' => 0,
    //是否显示在导航栏内？  1是，0否
    'show_nav' => 1,
    //模块描述
    'summary' => '学生成绩统计，适用于学校课程成绩统计管理',
    //开发者
    'developer' => '水月居科技有限公司',
    //开发者网站
    'website' => 'http://blog.sina.com.cn/shuiyueju8',
    //前台入口，可用U函数
    'entry' => 'Chengji/index/index',
    
    'admin_entry' => 'Admin/Chengji/index',

    'icon' => 'line-chart',

    'can_uninstall' => 1

);

