exportOneExcelByTitle
xlsdata=
[ data[0]=[
      【sheetname】=表名str
      【header】=头部标题行str
      【title】=标题行array
      【width】=列宽array
      【data】=数据列表 二维array
      【pageset】=页面设置array 
                 【orientation=页面是否横向】
                 【PaperSize=A4】 

       ]
范例
xlsdata=
array(1) {
  [0] => array(5) {
    ["sheetname"] => string(46) "2017学年第二学期学生变更情况汇总"
    ["header"] => array(1) {
      [0] => string(46) "2017学年第二学期学生变更情况汇总"
    }
    ["title"] => array(6) {
      [0] => string(6) "序号"
      [1] => string(6) "班级"
      [2] => string(6) "学号"
      [3] => string(6) "姓名"
      [4] => string(6) "性别"
      [5] => string(6) "备注"
    }
    ["width"] => array(6) {
      [0] => string(1) "5"
      [1] => string(1) "9"
      [2] => string(1) "5"
      [3] => string(2) "10"
      [4] => string(1) "4"
      [5] => string(2) "40"
    }
    ["data"] => array(33) {
      [0] => array(5) {
        ["bj_code"] => string(6) "201205"
        ["st_code"] => 01
        ["st_name"] => string(9) "许宇鹏"
        ["sex"] => string(3) "男"
        ["bz"] => string(12) "上海恒丰"
      }
      [1] => array(5) {
        ["bj_code"] => string(6) "201202"
        ["st_code"] => 02
        ["st_name"] => string(9) "陈宇涵"
        ["sex"] => string(3) "男"
        ["bz"] => string(27) "厦门同安区阳翟小学"
      }
      [2] => array(5) {
        ["bj_code"] => string(6) "201206"
        ["st_code"] => 03
        ["st_name"] => string(6) "徐浩"
        ["sex"] => string(3) "男"
        ["bz"] => string(27) "山西新绎县海泉学校"
      }
      [3] => array(5) {
        ["bj_code"] => string(6) "201702"
        ["st_code"] => 04
        ["st_name"] => string(9) "庞景燦"
        ["sex"] => string(3) "男"
        ["bz"] => string(21) "合肥外国语学校"
      }
      [4] => array(5) {
        ["bj_code"] => string(6) "201702"
        ["st_code"] => 05
        ["st_name"] => string(9) "王俊熙"
        ["sex"] => string(3) "男"
        ["bz"] => string(24) "福溪街道中心小学"
      }
      [5] => array(5) {
        ["bj_code"] => string(6) "201703"
        ["st_code"] => 06
        ["st_name"] => string(9) "郭宗燱"
        ["sex"] => string(3) "男"
        ["bz"] => string(21) "黑龙江（试读）"
      }

    }
  }
}