<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 * @author 水月居<zzl@ourstu.com>
 */

namespace Wckc\Controller;

use Think\Controller;


class BaseController extends Controller{
    
    function _initialize()
    {
        header("Content-Type:text/html;charset=utf-8"); 
        if (!is_login()) {
            $this->error('本模块必须登录后才能使用。',U('ucenter/member/login'));
        }

         $this->studentModel=D('Wckc/WckcStudent');        
         $this->jiemuModel=D('Wckc/WckcJiemu');       
         $this->courseModel=D('Wckc/WckcCourse');
         $this->classModel=D('StudentClass');
         $this->filesModel=D('Wckc/WckcFiles');
         $this->yearModel=D('Year');
         $this->termModel=D('YearTerm');
         $this->curTerm=I('term',$this->termModel->where('cur=1')->getfield('term'));
         $this->curYear=I('year',$this->yearModel->where('cur=1')->getfield('year'));
         
         $catTitle=modC('PAPER_CATEGORY_TITLE','五彩课程','Wckc');

        $sub_menu['left'][]= array('tab' => 'home', 'title' => $catTitle, 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','home');
      
    }




} 