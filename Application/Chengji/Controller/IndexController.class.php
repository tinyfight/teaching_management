<?php
namespace Chengji\Controller;
use Think\Page;
use Think\Upload;
use Think\Controller;

/**
 * 学生成绩控制器
 * @author水月居 <singliang@163.com>
 */
class IndexController extends Controller {

    function _initialize()
    {
        header("Content-Type:text/html;charset=utf-8"); 
        if (!is_login()) {
            $this->error('本模块必须登录后才能使用。',U('Home/Index/index'));
        }

         $this->studentModel=D('Student/StudentXs');        
         // $this->jiemuModel=D('Wckc/WckcJiemu');       
         // $this->courseModel=D('Wckc/WckcCourse');
         $this->classModel=D('StudentClass');
         // $this->filesModel=D('Wckc/WckcFiles');
         $this->scoreModel=D('Chengji/ChengjiStudentScore'); 

         $this->ROOTPATH= './Uploads/Chengji/';

         $this->yearModel=D('Year');
         $this->termModel=D('YearTerm');
         $this->curTerm=$this->termModel->where('cur=1')->getfield('term');
         $this->curYear=I('year',$this->yearModel->where('cur=1')->getfield('year'));
         $this->termlist=$this->termModel->field('term, term_ch')->select();
         $this->yearlist=$this->yearModel->field('year,year_ch')->select();

        $catTitle=modC('CATEGORY_TITLE','学生成绩统计','Chengji');
        $sub_menu['left'][]= array('tab' => 'course', 'title' => $catTitle, 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','course');
       
    }


    public function classScore($bj_name="请选择班级"){
        $kid=I('kid');
        $gmap=array(
          'kid'=>4,
          'grade'=>4
          );

        $subjects=M('ChengjiGrade')->where($gmap)
                 ->getField('subjects');
        $subject_list=str2arr($subjects);
        dump($subject_list);
        $arr = array(
    array(
       'user_id' => 100,
       'goods_id' => 10,
       'number' => 104,
    ),
    array(
       'user_id' => 100,
       'goods_id' => 10,
       'number' => 2,
    ),
    array(
       'user_id' => 101,
       'goods_id' => 10,
       'number' => 2,
    ),
    array(
       'user_id' => 100,
       'goods_id' => 10,
       'number' => 2,
    ),
);
$item=array();
foreach($arr as $k=>$v){
    if(!isset($item[$v['user_id']])){
        $item[$v['user_id']]=$v;
    }else{
        $item[$v['user_id']]['number']+=$v['number'];
    }
}
dump($item);die;
        $classModel=$this->classModel;
        $map=array(
           'year'=>$this->curYear
          );
        $clist=$classModel->field("id, bj_name")->where($map)->order(array('sort'=>"asc"))->select();
        //获取学生信息
         $bj_name = I('bj_name')?I('bj_name'):$bj_name;//获取班级id
         $map['class']=$bj_name;
         //dump($clist);
        $studentModel=$this->studentModel;
        $order=array('grade'=>'desc');
        $scoreModel=$this->scoreModel;
        $stlist=$scoreModel//->relation(true)
        //->field("sid,code,class,name,sex,cname,score,outstand,scoreuptime")
        ->order($order)->where($map)->limit('42')->select();
        dump($stlist);

        //获取当前班级信息
        if("请选择班级"==$bj_name){
            $cur['bj_name']="请选择班级";
             }else{
              $cur['bj_name']=$bj_name;
                //$cur=$XsClass->where(array('bj_name'=>$bj_name))->find();
            }
        // dump($map);
        //  dump($stlist);
        $this->assign('_cur', $cur);
        $this->assign('classlist', $clist);
        $this->assign('student', $stlist);
        $this->display();


     }


    /**
     * 课程管理首页
    */
    public function index(){

		
		//echo "查看已申报课程";
		$this->Model=D('WckcCourse');
		$where=array("status"=>"1","year"=>C('CUR_YEAR'));		
        //$curriculum=D("WckcCourse");
		$list=$this->Model
        //->field("sid, grade, class, number, name, sex, cid, status")
        ->order("category asc")->where($where)->select();

		$this->assign('curriculum', $list);
        $this->assign('categoryid', $cateid);
        $this->display('index');
    }
    /* *
     * 下载文件
    */
   public function downloadfile(){
    $id=I('fid', 0,'intval');
    $thisModel=$this->filesModel;
    $file=$thisModel->field('cid,file_md5,update_time',true)->find($id);
    $file_name=$file['filename'];
    $file_url=$this->ROOTPATH . $file['url'];    
    $thisModel->downLocalFile($file_url,$file_name,$file_type);
    die();
   }

	    /**
	     * 清除课程成绩
	    */
    public function deleteScore(){
		
		$this->Model=D('WckcStudent');
		$where=array("status"=>"1");
		$data=array(
			'score'=>'',
			'outstand'=>'0'
			);		
 
        $list=$this->Model        
        ->where($where)->setField($data);
        if($list){
             $this->error('数据清除失败！');
        }else{
        	$this->success('数据清除成功！',U('index')); 
        }
              
    }

        //课程程查询
     public function indexCourse(){    
   
        $where=array("status"=>"1","year"=>C('CUR_YEAR'));    
            $courseModel=$this->courseModel;
        $list= $courseModel
            //->field("sid, grade, class, number, name, sex, cid, status")
            ->order("category asc")->where($where)->select();
            // dump($list);die;
            
        
          //dump($category);
            //$cateid=array($category['id'],true);
            $this->assign('_curriculum', $list);
            $this->assign('_categoryid', $cateid);
           
            $this->meta_title = '课程信息';
            $this->display();
        }


     //课程程导出各类课程excel表格

     public function exportCourseExcel(){    
    
    
      $where=array("status"=>"1","year"=>C('CUR_YEAR'),'count'=>array('gt','0'));    
          $courseModel=$this->courseModel;
          $list= $courseModel
          //->field("sid, grade, class, number, name, sex, cid, status")
          ->order("room asc")->where($where)->select();
      

          $this->assign('data', $list);
         
         
          $this->meta_title = '课程信息';
          $this->display();
      }

    public function updateCount (){
    	$CourseModel=$this->courseModel;
    	$student=$this->studentModel;
        $map['year']=I('year')?I('year'):C('CUR_YEAR');
        $map['status']=1;
        $clist=$CourseModel->field("cid")->where($map)->select();
        foreach ($clist as $amap)
        {
        // dump($amap);die;
            $cur['count']=$CourseModel->where($amap)->getField('count');
            
            //检查重新统计人数
            $data['count']=$student->where($amap)->count();

            

            if($cur['count']!=$data['count']){

              $data['number']=$CourseModel->where($amap)->getField('number');
              $data['shengyu']=$data['number']-$data['count']; 
              $CourseModel->where($amap)->save($data);
              unset($data);
              echo  '更新ID号为'.$amap['cid'].'课程人数！<br/>'; 
            }else{
              $data['number']=$CourseModel->where($amap)->getField('number');
              $data['shengyu']=$data['number']-$data['count'];
              $CourseModel->where($amap)->save($data);
              unset($data);
 
            }
         } 
   
        //获取学生信息
       // die();
    	$this->success('人数更新成功！',U('indexCourse'),2);
    }

    
    public function showCourse($cid=0){
        $cid=I('cid')?I('cid'):$cid;//获取cid
        //dump($cid);
        $curriculum=D("XbCurriculum");
        $cur=$curriculum->find($cid);
        //dump($_cur);
        $this->assign('_cur', $cur);
        $this->display();
    }


	/**
	*编辑课程信息
	*/
    public function edit($cid=0){
        $cid=I('cid')?I('cid'):$cid;//获取cid
         $courseModel=$this->courseModel;
         if(IS_POST){
         	//dump(I('post.'));
            $data = $courseModel->create();
            //dump($data);die();

             $courseModel->save($data);
             $this->success('更新成功', U('Index/indexCourse'),1); 
             }       
        $cur=$courseModel->find($cid);
        //dump($cur);
        $this->assign('_cur', $cur);
        $this->display();             
    }

    public function find($page = 1, $keywords = '')
    {
        $nickname = op_t($keywords);
        if ($nickname != '') {
            $map['nickname'] = array('like','%'.$nickname.'%');
        }
        $list = D('Member')->where($map)->findPage(20);
        foreach ($list['data'] as &$v) {
            $v['user'] = query_user(array('avatar128', 'space_url', 'username', 'fans', 'following', 'signature', 'nickname'), $v['uid']);
        }
        unset($v);
        $this->assign('lists', $list);
        $this->assign('nickname',$nickname);
        $this->display();
    }



    /*显示课程资料汇总列表*/
    public function showUpload(){
		 $order=array('id'=>'desc','cid'=>'asc');
		 $map["cid"]=I('cid')?I('cid'):0;//获取cid
		 if(!$map["cid"]) unset($map["cid"]);
     $year=I('get.year');
		 $map['year']=$year?$year:C('CUR_YEAR');
     //dump($map);
    	$list=D('XbFiles')->field('cate,cid,cname,teacher,update_time,filename,url,uid')->where($map)->order($order)->select();
    	//dump($list);
    	$this->assign('list',$list);
    	$this->display();
    }

	/*编辑优秀学员*/
	public function outstand(){
         $cid=78;
		  $cid=I('cid')?I('cid'):$cid;//获取cid
		  $map['year']=C('YEAR');
		  $map['cid']=$cid;//$cid;
		  //获取当前课程学生
	     $studata=M('student')->where($map)->field('sid,class,code,name,sex,outstand')->select();
	     //dump($studata);
	     $this->assign('student',$studata);
	     //获取当前课程信息
		  $cur=D('Curriculum')->find($cid);
         $this->assign('_cur', $cur);
          if(IS_POST){
          	dump($_POST);
          }
          $this->display();

	}

}