<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author:  <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Chengji\Controller;
use Think\Controller;
/**
 * 空模块，主要用于显示404页面，请不要删除
 */
class EmptyController extends Controller{
	function _empty(){

    // $url = U('Chengji/Index/index');
    // dump($url);die;
    // header("Location:$url");

   $this->redirect('Chengji/Index/index', array('cate_id' => 2), 5, '页面跳转中...');	
	}

	//没有任何方法，直接执行HomeController的_empty方法
	//请不要删除该控制器
}
