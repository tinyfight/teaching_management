<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://blog.sina.com.cn/shuiyueju8/ All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水月居 <singliang@qq.com> <http://www.zjttwgy.com>
// +----------------------------------------------------------------------

/**
 * 用于生成excel文件的函数
 * author:walker
 * @param $data 生成excel的数据(二维数组形式)
 * @param null $savefile 生成excel的文件名(不指定,则为当前时间戳)
 */
require_once(APP_PATH . '/Jxgl/Common/excel.php');//用户自定义扩展函数
