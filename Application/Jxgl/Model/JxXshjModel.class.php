<?php
/**
 * 学生获奖登记模型
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午3:11
 * @author 水月居<singliang@163.com>
 */

namespace Jxgl\Model;


use Think\Model;

class JxXshjModel extends Model{

     protected $_validate = array(
          array('teacher', '1,20', '教师姓名长度不合法', self::EXISTS_VALIDATE, 'length'), 
          array('student', '1,20', '学生姓名长度不合法', self::EXISTS_VALIDATE, 'length'),         
        
    );
        protected $_auto = array(
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
        array('sort', '1', self::MODEL_INSERT),        
    );


    public function editData($data)
    {
        if($data['id']){
            $data['update_time']=time();
            $res=$this->save($data);
        }else{
            $data['create_time']=$data['update_time']=time();
            $res=$this->add($data);
        }
        return $res;
    }

    public function getData($id){
        return $this->find($id);
    }

    public function getListByPage($map,$page=1,$order='create_time desc,sort asc',$field='*',$r=20)
    {
        $totalCount=$this->where($map)->count();
        if($totalCount){
            $list=$this->where($map)->page($page,$r)->order($order)->field($field)->select();
        }
        return array($list,$totalCount,$r);
    }

    public function getList($map,$field='*',$order='sort asc')
    {
        $lists = $this->where($map)->field($field)->order($order)->select();
        return $lists;
    }
    public function deleteData($id)
    {
        //获取微博编号
        $adata= $this->find($id);
        if ($adata['status'] == -1) {
            return false;
        }
        $id = $adata['id'];
        //将记录标记为已经删除
        $this->where(array('id' => $id))->setField('status', -1);
        //返回成功结果
        return true;
    }
} 