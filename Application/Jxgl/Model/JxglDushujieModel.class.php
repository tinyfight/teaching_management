<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Jxgl\Model;
use Think\Model;
use Think\Page;

/**
 * 文档基础模型
 */
class JxglDushujieModel extends Model{
    protected $_validate = array(
        array('title', '1,100', '节目名称长度不合法', self::EXISTS_VALIDATE, 'length'),
        //array('content', '1,40000', '内容长度不合法', self::EXISTS_VALIDATE, 'length'),
    );

    protected $_auto = array(
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
        array('sort', '1', self::MODEL_INSERT),
        array('uid', 'is_login',3, 'function'),
    );
	
	/**
	 * 下载本地文件
	 * @param  array    $file     文件信息数组
	 * @return boolean            下载失败返回false
	 */
	public function downLocalFile($file){
		$fileurl=iconv("UTF-8","gb2312",$file['rootpath'].$file['url']);
		if(is_file($fileurl)){
			$file['size'] = filesize($fileurl); //获取文件大小
			/*兼容IE文件名中文设置*/
			$encoded_filename = rawurlencode($file['filename']);
            //$encoded_filename = str_replace("+", "%20", $encoded_filename);
			
			/* 执行下载 */ 
			$ua = $_SERVER["HTTP_USER_AGENT"]; 
			header("Content-Description: File Transfer");
			 
			header('Content-type: ' . $file['type']);
			header('Content-Length:' . $file['size']);
			if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
				//header('Content-Disposition: attachment; filename ="' . $encoded_filename . '"');
				header('Content-Disposition: attachment; filename ="' . rawurlencode($file['filename']) . '"');
			} else {
				header('Content-Disposition: attachment; filename="' . $file['filename'] . '"');
			}
			readfile($fileurl);
			exit;
		} else {
			$this->error = '文件已被删除！';
			return false;
		}
	}









}
