<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午3:11
 * @author 水月居<singliang@163.com>
 */

namespace Jxgl\Model;


use Think\Model;

class JxXshjCateModel extends Model{

     protected $_validate = array(
          array('dj', '1,20', '获奖等级为1-20个字符', self::EXISTS_VALIDATE, 'length'), 
          array('dj', '', '该获奖等级已经存在', self::EXISTS_VALIDATE, 'unique'),        
        
    );
        protected $_auto = array(
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
        
    );


    public function editData($data)
    {
        if($data['id']){
            $data['update_time']=time();
            $res=$this->save($data);
        }else{
            $data['create_time']=$data['update_time']=time();
            $res=$this->add($data);
        }
        return $res;
    }

    public function getData($id){
        return $this->find($id);
    }



    public function getList($map,$field='*',$order='sort asc')
    {
        $lists = $this->where($map)->field($field)->order($order)->select();
        return $lists;
    }
} 