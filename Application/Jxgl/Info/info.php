<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 18-9-28
 * Time: 下午01:12
 * @author 水月居<singliang@163.com>
 */


return array(
    //模块名
    'name' => 'Jxgl',
    //别名
    'alias' => '教学管理',
    //版本号
    'version' => '6.1.0',
    //是否商业模块,1是，0，否
    'is_com' => 0,
    //是否显示在导航栏内？  1是，0否
    'show_nav' => 1,
    //模块描述
    'summary' => '可以用于学校的教学管理',
    //开发者
    'developer' => '水月居科技有限公司',
    //开发者网站
    'website' => 'http://www.shuiyueju.com',
    //前台入口，可用U函数
    'entry' => 'Jxgl/Index/index',

    'admin_entry' => 'Admin/Jxgl/index',

    'icon' => 'briefcase',

    'can_uninstall' => 1

);