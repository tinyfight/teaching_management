<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 * @author 水月居<zzl@ourstu.com>
 */
namespace Jxgl\Controller;

use Think\Controller;

class LwfbController extends Controller
{
  public function _initialize()
    {
       header("Content-type: text/html; charset=utf-8");
       $uid=is_login();
       if($uid){
        $my=query_user(array('nickname'));
        $my['uid']=$uid;
        $this->assign(my,$my);               
          }else{
          $this->error('请先登录',U('Home/Index/index'),1);
        }  
        /*学期信息*/
        $this->yearModel=D('Year');
        $this->termModel=D('YearTerm');
        $this->termlist=$this->termModel->field('term, term_ch')->select();
        $this->curTerm=I('term',$this->termModel->where('cur=1')->getfield('term'));
        $this->curYear=I('year',$this->yearModel->where('cur=1')->getfield('year'));

        //准备公用数据列表
        $this->lwfbModel=D('Jxgl/JxglJslwfb');

        $sub_menu['left'][]= array('tab' => 'luwen', 'title' =>'论文上传', 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','luwen');
        $this->assign('now_table',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);
        $this->assign('now_nav','Jshj');

    }
/**
  * 获奖登记首页
 */
    public function index($page = 1)
    {
        $thisModel=$this->lwfbModel;
        if(IS_POST){

           // dump($_POST);exit;
             //$bz    =I('post.bz'); //备注
             $teacher  =I('post.teacher'); 
             $data=I('post.');
             // dump($data);die;
           if(!($teacher)){
            $this->error('作者姓名不能为空');
             }
             $bkmc=I('post.bkmc');
           if(!($bkmc)){
            $this->error('报刊名称必须填写');
             }

             

             $data=$thisModel->create($data);
             //dump($data);exit;

             $result=$thisModel->add($data); 
             if(!$result ){               
                  $this->error($thisModel->getError());
                }else{               
                $this->success('论文登记成功！'.$info, Cookie('__forward__'));               
                } 
           } 

        $map['status']=1;
        if(empty($aTeacher)){
           unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $aTeacher . '%');
          
        }
        $page=I('page',1);
        //dump($page);
        $order='create_time desc,sort asc';
        $r=I('r',C('LIST_ROWS'));

        $list=$thisModel->getListByPage($map,$page,$order,$field='*',$r);
       //dump($list);exit;
 
        $this->assign('list',$list['0']);
        $this->assign('totalPageCount',$list['1']);
        $this->assign('r',$stlist['2']);//每页几条记录
        $this->display();
    }


    /** 
    *发表论文汇总
    *
      */
    public function hz(){
       $thisModel=$this->lwfbModel;
      $aTeacher=I('teacher','');
      if(empty($aTeacher)){
        unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $aTeacher . '%');          
        } 
       $this->assign('ateacher',$aTeacher);
        $map['status']=1;
        $page=I('page',1, 'intval' );
        //dump($page);
        $order='teacher asc,sort desc';

        $r=I('r',C('LIST_ROWS'));
        $list=$thisModel->getListByPage($map,$page,$order,$field='*',$r);
       //dump($stlist);exit;
       
        $this->assign('list',$list['0']);
        $this->assign('totalPageCount',$list['1']);
        $this->assign('r',$list['2']);//每页几条记录 
       
        $this->display();
    }
   /*获奖信息修改*/
   public function edit(){
      $aId=I('id');
      $thisModel=$this->lwfbModel;
      $adata=$thisModel->getData($aId);
      $this->assign('data',$adata);
      //dump($data);
      //exit;
      if(IS_POST){
       // dump($_POST);
        $data=$thisModel->create($_POST);
             //dump($this->studentModel);
           // dump($data);
           // exit;
             $result=$thisModel->save($data); 
             if(!$result ){               
                  $this->error($thisModel->getError());
                }else{
               // echo '数据更新失败！';
                $this->success('更新成功',U('hz'));               
                } 
      }
      $this->display();
   }



       public function search(){

      $year=I('year',C('CUR_YEAR'));
      if($year=="所有学年"){
        unset ($map['$year']);
      }else{
        $map['year']=$year;//不同字段相同的查询条件       
        $this->assign('year',$year);        
      } 

      $teacher=I('teacher','');
      if(empty($teacher)){
        unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $teacher . '%');
          $this->assign('teacher',$teacher);
        } 
       $thisModel=$this->jshjModel;
        $map['status']=1;
        //dump($map);
        $list=$thisModel->where($map)->select();
        $this->assign('list',$list);
        $this->display('index');
      }
     

  /*导出教师获奖情况汇总excel文件*/
 /*导出课题汇总excel文件*/
  public function lwfbexcel(){
    $year=I('year',C(CUR_YEAR));
    $map['year']=$year;
    $map['status']=1;
    
    //论文发表数据查询
    $thisModel=$this->lwfbModel;
    $lWfbwidth=array('5','7','30','8','20','15','15','10');
    $lWfbtitle=array('序号','作者','文章标题','发表日期','报刊名称','国际刊号','国内刊号','邮发代号','备注');
    $lwfblist= $thisModel->field('teacher,title,date,bkmc,gjkh,gnkh,yfdh,bz')->where($map)->select();
   if(!is_array($lwfblist)){
    $list[]=array('',$year.'学年','暂无数据','','','');//没有数据设置无数据提示行防止溢出死机
   }else{
      $lwfblist=listAddID($lwfblist);//增加自动排序序号
   }
    $data[1]=array(
      'sheetname'=>'论文发表',
      'header'=>$year.'学年论文发表情况汇总',
      'title'=>$lWfbtitle,
      'width'=>$lWfbwidth,
      'data'=>$lwfblist,
      );

  //教师获奖 数据查询
   $jshjModel=$this->jshjModel;
    $width=array('5','7','8','8','30','9','13','8');
    $title=array('序号','教师','获奖日期','获奖类别','获奖内容','获奖等级','举办单位','备注');
    $jshjlist=$jshjModel->field('teacher,date,leibie,content,hjdj,jbdw,bz')->where($map)->select();
      if(!is_array($jshjlist)){
        $jshjlist[]=array('',$year.'学年','暂无数据','','','');
      } else{
        $jshjlist=listAddID($jshjlist);//增加自动排序序号
      }  
     $data[2]=array(
        'sheetname'=>'教师获奖汇总',
        'header'=>$year.'学年 教师获奖情况汇总表',
        'title'=>$title,
        'width'=>$width,
        'data'=>$jshjlist,
        );


//dump($data);exit;
    $savefile=$year."学年教师获奖及论文发表汇总".date(md);
    //输出文档函数在common/function.php中
    exportExcel($data,$savefile);

  }


} 
