<?php
namespace Admin\Controller;

use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminListBuilder;

class JxglController extends AdminController
{
    
    // protected $ketiModel;
    // protected $keticateModel;

    function _initialize()
    {
        parent::_initialize();
        header("Content-type: text/html; charset=utf-8");
        $this->yearModel=D('Year');
        $this->termModel=D('YearTerm');
        $this->termlist=$this->termModel->field('term, term_ch')->select();
        $this->yearlist=$this->yearModel->field('year, year_ch')->select();
        $this->curTerm=I('term',$this->termModel->where('cur=1')->getfield('term'));
        $this->curYear=I('year',$this->yearModel->where('cur=1')->getfield('year'));

        $this->jshjModel = D('Jxgl/JxglJshj');        
        $this->CateModel = D('Jxgl/JxglJshjCategory');
        $this->xshjModel = D('Jxgl/JxglXshj');        
        $this->xsCateModel = D('Jxgl/JxglXshjCate');
        $this->lwfbModel=D('Jxgl/JxglJslwfb');

    }
    public function index(){
        //getListByPage($map,$page=1,$order='sort asc,create_time desc',$field='*',$r=20)

        $page=I('page',1);
        $r=I('r',10);
        $year=I('year',$this->curYear);
        // $year=2017;
        // $map=array(
        //     'status'=>array('egt',0),
        //     'year'=>$year
        //     );
        // $count['beike']=$this->beikeModel->where($map)->count();
        // $count['shiti']=$this->shitiModel->where($map)->count();
        // $data[]=array(
        //     'title'=>" 出卷命题",
        //     'count'=>$count['shiti'],
        //     );
        // $count['gongkaike']=$this->gongkaikeModel->where($map)->count();
        // $data[]=array(
        //     'title'=>"公开课",
        //     'count'=>$count['gongkaike'],
        //     );
       
        // $count['zhoumo']=$this->zhoumolianxiModel->where($map)->count();
        // $data[]=array(
        //     'title'=>"周末练习",
        //     'count'=>$count['zhoumo'],
        //     );

            if(IS_POST){
                $count_day=I('post.count_day', C('COUNT_DAY'),'intval',7);
                if(M('Config')->where(array('name'=>'COUNT_DAY'))->setField('value',$count_day)===false){
                    $this->error(L('_ERROR_SETTING_').L('_PERIOD_'));
                }else{
                   S('DB_CONFIG_DATA',null);
                    $this->success(L('设置成功').L('_PERIOD_'),'refresh');
                }

            }else{
                $this->meta_title = L('_INDEX_MANAGE_');
                $today = date('Y-m-d', time());
                $today = strtotime($today);
                $count_day = C('COUNT_DAY',null,7);
                $count['count_day']=$count_day;
                for ($i = $count_day; $i--; $i >= 0) {
                    $day = $today - $i * 86400;
                    $day_after = $today - ($i - 1) * 86400;
                    $week_map=array('Mon'=>L('_MON_'),'Tue'=>L('_TUES_'),'Wed'=>L('_WEDNES_'),'Thu'=>L('_THURS_'),'Fri'=>L('_FRI_'),'Sat'=>'<strong>'.L('_SATUR_').'</strong>','Sun'=>'<strong>'.L('_SUN_').'</strong>');
                    $week[] = date('m月d日 ', $day). $week_map[date('D',$day)];
                    $user = UCenterMember()->where('status=1 and reg_time >=' . $day . ' and reg_time < ' . $day_after)->count() * 1;
                    $registeredMemeberCount[] = $user;
                    if ($i == 0) {
                        $count['today_user'] = $user;
                    }
                }
                $week = json_encode($week);
                $this->assign('week', $week);
                $count['total_user'] = $userCount = UCenterMember()->where(array('status' => 1))->count();
                $count['today_action_log'] = M('ActionLog')->where('status=1 and create_time>=' . $today)->count();
                $count['last_day']['days'] = $week;
                $count['last_day']['data'] = json_encode($registeredMemeberCount);
                // dump($count);exit;

                $this->assign('count', $count);
                $this->display('Jxgl@Admin/index');
            }
        
    }


    /**
     * 设置软件分类状态：删除=-1，禁用=0，启用=1
     * @param $ids
     * @param $status
     * @author 水月居<singliang@163.com>
     */
    public function setCategoryStatus($ids, $status)
    {
        !is_array($ids)&&$ids=explode(',',$ids);
        if($status==-1){
            if(in_array(1,$ids)){
                $this->error('id为 1 的分类是基础分类，不能被删除！');
            }
            $map['cate']=array('in',$ids);
            $this->softwareModel->where($map)->setField('cate',1);
        }
        $builder = new AdminListBuilder();
        $builder->doSetStatus('SoftwareCate', $ids, $status);
    }

    //分类管理end
}