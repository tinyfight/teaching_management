<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 * @author 水月居<zzl@ourstu.com>
 */
namespace Jxgl\Controller;

use Think\Controller;

class JshjController extends Controller
{
      function _initialize()
    {
        header("Content-Type:text/html;charset=utf-8"); 
        if (!is_login()) {
            $this->error('本模块必须登录后才能使用。',U('ucenter/member/login'));
        }
        /*学期信息*/
        $this->yearModel=D('Year');
        $this->termModel=D('YearTerm');
        $this->termlist=$this->termModel->field('term, term_ch')->select();
        $this->curTerm=I('term',$this->termModel->where('cur=1')->getfield('term'));
        $this->curYear=I('year',$this->yearModel->where('cur=1')->getfield('year'));
        /*数据库*/
        $this->jshjModel = D('Jxgl/JxglJshj');        
        $this->CateModel = D('Jxgl/JxglJshjCategory');
        $this->xshjModel = D('Jxgl/JxglXshj');        
        $this->xsCateModel = D('Jxgl/JxglXshjCate');
        $this->lwfbModel=D('Jxgl/JxglJslwfb');
        $catTitle=modC('PAPER_CATEGORY_TITLE','教学管理','Jxgl');
        $sub_menu['left'][]= array('tab' => 'home', 'title' => $catTitle, 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','home');
        $this->assign('now_table',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);
    }
/**
  * 获奖登记首页
 */
    public function index($page = 1)
    {
        $thisModel=$this->jshjModel;
        if(IS_POST){
             // dump($_POST);exit;
           $data=I('post.');
            
           $teacher  =I('post.teacher');              
           if(!($teacher)){
            $this->error('教师姓名不能为空');
             }
             
             $jbdw=I('post.jbdw');
           if(!($jbdw)){
            $this->error('举办单位必须填写');
             }
             /*传递参数获取相关获奖等级参数*/
           $cid=I('post.hjdj');
           $hjdj=$this->CateModel->find($cid);
           $data['dj_id']=$hjdj['id'];//获奖等级id
           $data['jlje']=$hjdj['je'];//奖励金额
           $data['hjdj']=$hjdj['lb'];//获奖等级
           $data['year']=$data['year']?$data['year']:C(CUR_YEAR);
           $data['sort']=1;

             $data=$thisModel->create($data);
             // dump($data);exit;

             $result=$thisModel->add($data); 
             if(!$result ){               
                  $this->error($thisModel->getError());
                }else{               
                $this->success('获奖登记成功！'.$info, U('index'));               
                } 
           } 

        $map['status']=1;
        $page=I('page',1);
        //dump($page);
        $order='create_time desc,sort asc';
        $r=I('r',C('LIST_ROWS'));

        $list=$thisModel->getListByPage($map,$page,$order,$field='*',$r);
       //dump($list);exit;
        $clist=$this->CateModel->order('sort asc')->select();
        $this->assign('clist',$clist);
        $this->assign('list',$list['0']);
        $this->assign('totalPageCount',$list['1']);
        $this->assign('r',$stlist['2']);//每页几条记录
        $this->display();
    }
    public function search(){

      $year=I('year',C('CUR_YEAR'));
      if($year=="所有学年"){
        unset ($map['$year']);
      }else{
        $map['year']=$year;//不同字段相同的查询条件       
        $this->assign('year',$year);        
      } 

      $teacher=I('teacher','');
      if(empty($teacher)){
        unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $teacher . '%');
          $this->assign('teacher',$teacher);
        } 
       $thisModel=$this->jshjModel;
        $map['status']=1;
        //dump($map);
        $list=$thisModel->where($map)->select();
        $this->assign('list',$list);
        $this->display('index');
      }

    /** 
    *获奖汇总
    *
      */
    public function hjhz(){

      $teacher=I('teacher','');
      if(empty($teacher)){
        unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $teacher . '%');
          $this->assign('teacher',$$teacher);
        } 
        $thisModel=$this->jshjModel;
        $map['status']=1;
        $page=I('page',1, 'intval' );
        //dump($page);
        $order='teacher asc,sort desc';

         // $order='sort asc,create_time desc';
        $r=I('r',C('LIST_ROWS'));
        $stlist=$thisModel->getListByPage($map,$page,$order,$field='*',$r);
       //dump($stlist);exit;
       
        $this->assign('list',$stlist['0']);
        $this->assign('totalPageCount',$stlist['1']);
        $this->assign('r',$stlist['2']);//每页几条记录 
        $this->display();
    }
   /*获奖信息修改*/
   public function edit(){
      $aId=I('id');
      $thisModel=$this->jshjModel;
      $adata=$thisModel->getData($aId);
      $this->assign('data',$adata); 
      $clist=$this->CateModel->order('sort asc')->select();
      $this->assign('clist',$clist);
      $cid=I('post.hjdj');
      $hjdj=$this->CateModel->find($cid);
      // dump($hjdj);
//'dj_id'    =>$hjdj['id'],
      if(IS_POST){

        $data=I('post.');
        // $cid=I('post.hjdj');
        // $res=$this->CateModel->find($cid);
        $data['dj_id']=$hjdj['id'];
        $data['hjdj']=$hjdj['lb'];
        $data['jlje']=$hjdj['je'];
         
        $data=$thisModel->create($data);
      //        dump($data);dump($_POST);
     
      // exit;
           // dump($data);exit;
             $result=$thisModel->save($data); 
             if(!$result ){               
                  $this->error($thisModel->getError());
                }else{
               // echo '数据更新失败！';
                $this->success('更新成功', U('index'));               
                } 
      }
      $this->display();
   }

    




  /*导出教师获奖情况汇总excel文件*/

     public function excel_1($year=""){
      $year=I('year')?I('year'):C('CUR_YEAR');
      $map["status"] =array('EGT',1);
      $map['year']=$year;

        $Model=$this->jshjModel;
        $order=array(
            'teacher'=>'asc',
            'sort' =>'asc',
          );
       
        $data=$Model->field('create_time,update_time,year',true)
                    ->where($map)->order($order)->select();
         //dump($data);exit;
       $count=count($data);
       $m=$count+2;//获取记录数+2
       //设置顶行标题
     $toptitle = $year.'学年 教师获奖情况汇总表';
      
        $xlsTitle = iconv('utf-8', 'gb2312', "学年教师获奖汇总");//设置文件名称
        $fileName = $year.$xlsTitle.date('_Ymd');//or $xlsTitle 文件名称可根据自己情况设定

        vendor("PHPExcel");  
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
    //设置当前工作表为$objActSheet
    $objActSheet = $objPHPExcel->getActiveSheet();
        $objActSheet ->setTitle(date(Y).'学年教师获奖汇总');//设置表标题 
        //$objActSheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); //设置为A4纸大小
        $objActSheet ->setCellValue('A1',$toptitle)->mergeCells('A1:H1');//mergeCells合并单元格   
        $objActSheet ->getStyle( 'A1')->getFont()->setSize(18);
    
    /*普通样式设置
    *设置表格线为细线--细边框BORDER_THICK边框是粗的*/
           $styleArray = array(  
                  'borders' => array(  
                     'allborders' => array(                 
                          'style' => \PHPExcel_Style_Border::BORDER_THIN,
                     ),
                   ),
                   'font' => array(
                    'bold' => false,
                    'color' => array('rgb' => '000000'),
                    'size' => 9,
                    'name' => '宋体'
                   ),
                 );

 
        $objActSheet ->getStyle('A2:H'.$m)->applyFromArray($styleArray );
        $objActSheet ->getStyle( 'A2')->getAlignment()->setShrinkToFit(true);//设置自动缩小字体填充;
        $objActSheet ->getStyle('A1:H2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
          ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
        $objActSheet ->getStyle( 'A1:H2')->getFont()->setBold(true);//设置顶端标题为粗体
        //设置单元格宽度
        $objActSheet ->getColumnDimension('A')->setWidth(5);//序号宽度
        $objActSheet ->getColumnDimension('B')->setWidth(8);//教师
        $objActSheet ->getColumnDimension('C')->setWidth(8);
        $objActSheet ->getColumnDimension('D')->setWidth(8);
        $objActSheet ->getColumnDimension('E')->setWidth(28);
        $objActSheet ->getColumnDimension('F')->setWidth(9);
        $objActSheet ->getColumnDimension('G')->setWidth(13);
        $objActSheet ->getColumnDimension('H')->setWidth(8);


        $objActSheet ->getStyle('A:A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
        $objActSheet ->getStyle('A:I')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
    //标题行设置
        $objActSheet ->getStyle('A2:I2')->getAlignment()->setWrapText(true);//设定为自动换行
        $objActSheet ->getStyle('D2')->getAlignment()->setWrapText(true);
        $objActSheet ->getStyle( 'A2:I2')->getFont()->setSize(9);
        $objActSheet ->setCellValue('A2',"序号");
        $objActSheet ->setCellValue('B2',"教师");
        $objActSheet ->setCellValue('C2',"获奖日期");
        $objActSheet ->setCellValue('D2',"获奖类别");
        $objActSheet ->setCellValue('E2',"获奖内容");
        $objActSheet ->setCellValue('F2',"获奖等级");
        $objActSheet ->setCellValue('G2',"举办单位");
        $objActSheet ->setCellValue('H2',"备注");
               
       //以下输出内容
        $baseRow = 3;      //指定插入到第3行后
        foreach($data as $k => $v){         
         $key=$k+1;         //$key是输出顺序号
        $num=$baseRow+$k; //$num是循环操作行的行号
        //Excel的第A 列，uid是你查出数组的键值，下面以此类推
        $objActSheet ->setCellValue('A'.$num, $key )
           ->setCellValue('B'.$num, ' '.$v['teacher'])
           ->setCellValue('C'.$num, $v['date'])
           ->setCellValue('D'.$num, $v['leibie'])
           ->setCellValue('E'.$num, $v['content'])
           ->setCellValue('F'.$num, $v['hjdj'])
           ->setCellValue('G'.$num, $v['jbdw'])
           ->setCellValue('H'.$num, $v['bz']);
          }
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        header("Content-Transfer-Encoding:binary");
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');         
        $objWriter->save('php://output'); 
        exit; 
        }



    /*导出教师获奖情况带金额汇总excel文件*/

         public function excel_2($year=""){
      $year=I('year')?I('year'):C('CUR_YEAR');
      $map["status"] =array('EGT',1);
      $map['year']=$year;

        $Model=$this->jshjModel;
        $order=array(
            'teacher'=>'asc',
            'sort' =>'asc',
          );
       
        $data=$Model->field('create_time,update_time,year',true)
                    ->where($map)->order($order)->select();
         //dump($data);exit;
       $count=count($data);
       $m=$count+2;//获取记录数+2
       //设置顶行标题
     $toptitle = $year.'学年 教师获奖情况汇总表';
      
        $xlsTitle = iconv('utf-8', 'gb2312', "学年教师获奖汇总");//设置文件名称
        $fileName = $year.$xlsTitle.date('_Ymd');//or $xlsTitle 文件名称可根据自己情况设定

        vendor("PHPExcel");  
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
    //设置当前工作表为$objActSheet
    $objActSheet = $objPHPExcel->getActiveSheet();
        $objActSheet ->setTitle(date(Y).'学年教师获奖汇总');//设置表标题 
        //$objActSheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); //设置为A4纸大小
        $objActSheet ->setCellValue('A1',$toptitle)->mergeCells('A1:I1');//mergeCells合并单元格   
        $objActSheet ->getStyle( 'A1')->getFont()->setSize(18);
    
    /*普通样式设置
    *设置表格线为细线--细边框BORDER_THICK边框是粗的*/
           $styleArray = array(  
                  'borders' => array(  
                     'allborders' => array(                 
                          'style' => \PHPExcel_Style_Border::BORDER_THIN,
                     ),
                   ),
                   'font' => array(
                    'bold' => false,
                    'color' => array('rgb' => '000000'),
                    'size' => 9,
                    'name' => '宋体'
                   ),
                 );


        $objActSheet ->getStyle('A2:I'.$m)->applyFromArray($styleArray );
        $objActSheet ->getStyle( 'A2')->getAlignment()->setShrinkToFit(true);//设置自动缩小字体填充;
        $objActSheet ->getStyle('A1:I2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
          ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
        $objActSheet ->getStyle( 'A1:I2')->getFont()->setBold(true);//设置顶端标题为粗体
        //设置单元格宽度
        $objActSheet ->getColumnDimension('A')->setWidth(5);//序号宽度
        $objActSheet ->getColumnDimension('B')->setWidth(7);//教师
        $objActSheet ->getColumnDimension('C')->setWidth(8);
        $objActSheet ->getColumnDimension('D')->setWidth(8);
        $objActSheet ->getColumnDimension('E')->setWidth(26);
        $objActSheet ->getColumnDimension('F')->setWidth(8);
        $objActSheet ->getColumnDimension('G')->setWidth(12);
        $objActSheet ->getColumnDimension('H')->setWidth(8);
        $objActSheet ->getColumnDimension('I')->setWidth(6);

        $objActSheet ->getStyle('A:A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
        $objActSheet ->getStyle('A:I')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
    //标题行设置
        $objActSheet ->getStyle('A2:I2')->getAlignment()->setWrapText(true);//设定为自动换行
        $objActSheet ->getStyle('D2')->getAlignment()->setWrapText(true);
        $objActSheet ->getStyle( 'A2:I2')->getFont()->setSize(9);
        $objActSheet ->setCellValue('A2',"序号");
        $objActSheet ->setCellValue('B2',"教师");
        $objActSheet ->setCellValue('C2',"获奖日期");
        $objActSheet ->setCellValue('D2',"获奖类别");
        $objActSheet ->setCellValue('E2',"获奖内容");
        $objActSheet ->setCellValue('F2',"获奖等级");
        $objActSheet ->setCellValue('G2',"举办单位");
        $objActSheet ->setCellValue('H2',"奖励金额");
        $objActSheet ->setCellValue('I2',"备注");

        
       //以下输出内容
        $baseRow = 3;      //指定插入到第3行后
        foreach($data as $k => $v){         
         $key=$k+1;         //$key是输出顺序号
        $num=$baseRow+$k; //$num是循环操作行的行号
        //Excel的第A 列，uid是你查出数组的键值，下面以此类推
        $objActSheet ->setCellValue('A'.$num, $key )
           ->setCellValue('B'.$num, ' '.$v['teacher'])
           ->setCellValue('C'.$num, $v['date'])
           ->setCellValue('D'.$num, $v['leibie'])
           ->setCellValue('E'.$num, $v['content'])
           ->setCellValue('F'.$num, $v['hjdj'])
           ->setCellValue('G'.$num, $v['jbdw'])
           ->setCellValue('H'.$num, $v['jlje'])
           ->setCellValue('I'.$num, $v['bz']);

          }
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        header("Content-Transfer-Encoding:binary");
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');         
        $objWriter->save('php://output'); 
        exit; 
        }


} 
