<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 * @author 水月居<singliang@163.com>
 */

namespace Jxgl\Controller;


use Think\Controller;

class LunwenController extends BaseController
{
    public function _initialize()
    {
       header("Content-type: text/html; charset=utf-8");
       $uid=is_login();
       if($uid){
        $my=query_user(array('nickname'));
        $my['uid']=$uid;
        $this->assign(my,$my);               
          }else{
          $this->error('请先登录',U('Home/Index/index'),1);
        }  
        
        //准备公用数据列表	
        $this->lunwenModel=D('Jxgl/JxglLunwen');
        $this->ROOTPATH='./Uploads/Lunwen/';        
        C('_ROOTPATH','./Uploads/Lunwen/');
        $sub_menu['left'][]= array('tab' => 'luwen', 'title' =>'论文上传', 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','luwen');
        $this->assign('now_table',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);
    }

    public function index()
    {
        $url=U('Jxgl/Lunwen/viewLunwen');
      header("Location:$url");
    }

 public function uploadLunwen(){
 
      $lunwen=$this->lunwenModel;
	    $order=array('update_time'=>'desc','subject'=>'desc');
	    $lunwenlist=$lunwen->order($order)->limit('0,5')->select();
		$this->assign('list', $lunwenlist);


		if(IS_POST){
			$subject=I('post.subject');

            if('请选择学科类别'==$subject||empty($subject)){
                $this->error("请选择上传类别后再上传");
            }
            $title=I('post.title','','trimall');//过滤掉文章标题中的空格

            if(!$title) $this->error("文章标题必须正确填写");
   
        $save= I('subject').I('teacher').$title.date(ymd);
        $config = array(
            'maxSize'    =>   30*1024*1024, // 设置附件上传大小
            'rootPath'   => './Uploads/',
            'savePath'   =>     'Lunwen/',
            'saveName'   =>   $save, //iconv('utf-8', 'gbk', $save),已修改上传内核无需再转码
            'exts'       =>    array('jpg', 'gif', 'png', 'jpeg', 'zip', 'rar', 'doc', 'docx'),
            'autoSub'    =>    true,
            'subName'    =>    array('date','Y'),
         );
          $upload = new \Think\Upload($config);// 实例化上传类 
          $info   =  $upload->upload();

          $file=$info['upfile'];
				 
				 $file['ext']    =   pathinfo($_FILES['upfile']['name'], PATHINFO_EXTENSION);

				    if(!$info) {// 上传错误提示错误信息
				        $this->error($upload->getError());
				    }else{
				    // 上传成功,保存表单数据
				      
				      $url= date('Y').'/'.$file['savename'];
				        
				    	$data=array(    
				    		         "url"       => $url,
				    		         'subject'   => I('subject'),
				                     "teacher"   => I('teacher'),
				                     "filename"  => $_FILES['upfile']['name'],
				                     "title"     => $_POST["title"],  
				                     "uid"       => I('uid'),
				                     "year"      => date('Y'),
				                     'abstract'  => I('abstract')
				                    
				            ); 
			             if($lunwen->create($data)){
			                $rs=$lunwen->add(); 
			             }
			             if(!$rs){
			             	$this->error($lunwen->getError(),U('uploadLunwen'));

				         // dump($lunwen->getLastSql());
			             }else{
			             	$this->success('上传成功！');
			             }

				        
				      }
             } 
			
           $this->display();

	}
	public function editLunwen(){
		$id=I('id');
		$data=$this->lunwenModel->find($id);
		//dump($data);die;
		$this->assign('data',$data);
		if(IS_POST){
			$data=array(
				'id'=>I('id'),
				'subject'=> I('subject'),
				'teacher'=>I('teacher'),
				'abstract'=>I('abstract'),
				'uid' => is_login(),
				'title'=>I('title')
				);
			$data=$this->lunwenModel->create($data);
			$res=$this->lunwenModel->save();
			if($res){
				$this->success('论文信息修改成功！',U('Lunwen/viewLunwen'));
			}else{
				$this->error('论文信息修改失败！',U('Lunwen/editLunwen',array('id'=>$id)));
			}
			dump($res);die;
					}
		$this->display();
	}
        
  public function viewLunwen(){
  	   	/**查询字段条件拼组**/ 
		    $map['status']=1;
		    $aYear=I('year',date('Y'));
		    $this->assign('year',$aYear);
		    $map['year']=$aYear;

		    $aKeywords=I('keywords');


		    if(!empty($aKeywords)){
		      $map['title'] = array('like','%'.$keywords.'%');
		      $this->assign('keywords',$aKeywords);
		    }else{
		      unset($map['keywords']);
		    }
		   $subject=I('post.subject','所有学科');
		   $this->assign('subject',$subject);
		   if('所有学科'==$subject||empty($subject)){
		    unset($map['subject']);
		   }else{
		    $map['subject']=$subject;
		   
		   }
		   //dump($map);die;

 
        $lunwen=$this->lunwenModel;
	    $order=array('update_time'=>'desc','subject'=>'desc');
	    $lunwenlist=$lunwen->order($order)->where($map)->select();
		$this->assign('list', $lunwenlist);
        $this->display();
	}

    



} 