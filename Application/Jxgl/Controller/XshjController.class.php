<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 * @author 水月居<zzl@ourstu.com>
 */
namespace Jxgl\Controller;

use Think\Controller;

class XshjController extends Controller
{
	function _initialize()
    { 
    	$this->yearModel=D('Year');
        $this->termModel=D('YearTerm');
        $this->curTerm=I('term',$this->termModel->where('cur=1')->getfield('term'));
        $this->curYear=I('year',$this->yearModel->where('cur=1')->getfield('year'));
        $this->yearlist=$this->yearModel->field('year')->order('sort desc')->select();
        $this->termlist=$this->termModel->field('term,term_ch,term_chj')->order('sort desc')->select();
        $this->jshjModel = D('Jxgl/JxglJshj');        
        $this->CateModel = D('Jxgl/JxglJshjCategory');
        $this->xshjModel = D('Jxgl/JxglXshj');        
        $this->xsCateModel = D('Jxgl/JxglXshjCate');
        $this->lwfbModel=D('Jxgl/JxglJslwfb');

        $catTitle=modC('PAPER_CATEGORY_TITLE','教学管理','Jxgl');
        $sub_menu['left'][]= array('tab' => 'home', 'title' =>'学生获奖', 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','home');
        $this->assign('now_table',ACTION_NAME);
        $this->assign('now_nav',CONTROLLER_NAME);
    }
/**
  * 学生获奖登记
 */
    public function index($page = 1)
    {
        $thisModel=$this->xshjModel;
        if(IS_POST){
             // dump($_POST);exit;
             $bz    =I('post.bz'); //备注
             $student=I('post.student','');
            if(!($student)){
            $this->error('学生姓名必须填写');
             }
             $teacher  =I('post.teacher'); 
             $date=I('post.date');
           if(!($teacher)){
            $this->error('指导教师姓名必须填写');
             }
             $jbdw=I('post.jbdw');
           if(!($jbdw)){
            $this->error('举办单位必须填写');
             }
             /*传递参数获取相关获奖等级参数*/
           $cid=I('post.hjdj');
           $hjdj=$this->xsCateModel->find($cid);
          // dump($hjdj);

            $data=array(
             'leibie'   =>I('post.leibie'),
             'date'     =>$date,//获奖日期
             'teacher'  =>$teacher,
             'class'    =>I('post.class'),
             'student'  =>$student,           
             'content'  =>I('post.content'),
             'hjdj'     =>$hjdj['dj'],//获奖等级
             'jbdw'     =>$jbdw,//举办单位
             'jlje'     =>$hjdj['je'],//奖励金额
             'leibie'   =>I('post.leibie'),
             'content'  =>I('post.content'),            
             'year'     =>C(CUR_YEAR),
             'sort'     =>1,
             'bz'       =>$bz,
              );
          
             $data=$thisModel->create($data);
          // dump($data);exit;

             $result=$thisModel->add($data); 
             if(!$result ){               
                  $this->error($thisModel->getError());
                }else{               
                $this->success('获奖登记成功！'.$info, Cookie('__forward__'));               
                } 
           } 

        $map['status']=1;
        $page=I('page',1);
        //dump($page);
        // $order=array('create_time'=> 'desc',
        // 	         'sort'   =>'asc');
        $order='create_time desc,sort asc';

        $r=I('r',C('LIST_ROWS'));

        $list=$thisModel->getListByPage($map,$page,$order,$field='*',$r);
        //dump($list);exit;
        $classlist=D('XsClass')->field('id,bj_name,bj_name2')->select();
       //dump($list);exit;
        $clist=$this->xsCateModel->where($map)->order('sort asc')->select();
        $this->assign('classlist',$classlist);
        $this->assign('clist',$clist);
        $this->assign('list',$list['0']);
        $this->assign('totalPageCount',$list['1']);
        $this->assign('r',$list['2']);//每页几条记录
        $this->display();
    }
    public function search(){

      $map['teacher']=I('teacher','');
      if(empty($map['teacher'])){
        unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $map['teacher'] . '%');
          $this->assign('teacher', $map['teacher']);
        } 
        $thisModel=$this->xshjModel;
        $map['status']=1;
        $r=I('r',20);//C('LIST_ROWS')
        $order='date desc,teacher asc,sort desc';
        //dump($map);
        $list=$thisModel->getListByPage($map,$page,$order,$field='*',$r);
        $classlist=D('XsClass')->field('id,bj_name,bj_name2')->select();
       //dump($list);exit;
        $clist=$this->xsCateModel->where($map)->order('sort asc')->select();
        $this->assign('classlist',$classlist);
        $this->assign('clist',$clist);
        
        $this->assign('list',$list['0']);
        $this->display('index');
      }
    /** 
    *获奖等级相关设置
    *
      */
    public function hjhz(){

     
      $map['year']=I('year',$this->curYear);
      if(empty($map['year'])){
       unset ($map['$year']);//isset($map['year']) 
        $thisyear='';
      }else{
      	$thisyear=$map['year'];          
      }
      $this->assign('year',$map['year']);
      $map['student'] = I('st_name','');
      if(empty($map['student'])){
        unset($map['student']);
        }else{
          $map['student']=array('like', '%' . $map['student'] . '%');
          $this->assign('st_name',$map['student']);
        }
 
      $teacher=I('teacher','');
      if(empty($teacher)){
        unset ($map['teacher']);
        }else{
          $map['teacher']=array('like', '%' . $teacher . '%');
          $this->assign('teacher',$teacher);
        } 
        $thisModel=$this->xshjModel;
        $map['status']=1;
        $page=I('page',1, 'intval' );

        $order='date desc,teacher asc,sort desc';
        
        $r=I('r',20);
        $stlist=$thisModel->getListByPage($map,$page,$order,$field='*',$r);

        $this->assign('list',$stlist['0']);
        $this->assign('totalPageCount',$stlist['1']);
        $this->assign('r',$stlist['2']);//每页几条记录 
        $this->display();
    }
    /** 
    *获奖等级设置
    *
      */
    public function cate(){
      $thisModel=$this->xsCateModel;
        if(IS_POST){
             // dump($_POST);exit;
             if(!$_POST['dj']){
              $this->error('获奖等级必须填写');
             }
             $data=$thisModel->create($_POST);
             if(!$data){ $this->error($thisModel->getError());}
             //dump($data);exit;
             $res=$thisModel->editData($data);
             if(!$res){
              $this->error($thisModel->getError());
             }else{
             $this->success('更新成功', Cookie('__forward__')); 	
             }
           }

        $map['status']=1;
        $order='sort asc';
        $list=$thisModel->order($order)->select();
        $this->assign('list',$list);        
        $this->display();
    }
    /*删除获奖等级类别*/
    public function deletecate(){
      $id=I('id');
      $thisModel=$this->xsCateModel;
      $res=$thisModel->delete($id);
	  if(!$res){
	      $this->error($thisModel->getError());
	     }else{
	     $this->success('删除成功', Cookie('__forward__')); 	
	     }

    }
    /*删除获奖等级类别*/
    public function delete(){
      $id=I('id');
      $thisModel=$this->xshjModel;
      $res=$thisModel->deleteData($id);
    if(!$res){
        $this->error($thisModel->getError());
       }else{
       $this->success('删除成功', Cookie('__forward__'));   
       }

    }
   /*获奖信息修改*/
   public function edit(){
      $aId=I('id');
      $thisModel=$this->xshjModel;
      $adata=$thisModel->getData($aId);
      $this->assign('data',$adata);
      //dump($data);
      //exit;
      if(IS_POST){
       // dump($_POST);
        $data=$thisModel->create($_POST);
             //dump($this->studentModel);
           //dump($data);
           //exit;
             $result=$thisModel->save($data); 
             if(!$result ){               
                  $this->error($thisModel->getError());
                }else{
               // echo '数据更新失败！';
                $this->success('更新成功', Cookie('__forward__'));               
                } 
      }
      $this->display();
   }

 public function excel_out($year=''){
      $year=I('year',C(CUR_YEAR));
    $map['year']=$year;
    $map['status']=1;
    $thisModel=$this->xshjModel;   
    $order=array('teacher'=>'asc','date'=>'asc');
    //学生数据查询
     $list=$thisModel
          ->field('class,student,date,content,hjdj,jbdw,teacher,bz')
          ->order($order)->where($map)
          ->select();
     // dump($list);die;
     $width=array('4','8','12','8','30','9','12','6','6');
     $title=array('序号','班级','姓名','获奖日期','获奖内容','获奖等级','举办单位','指导教师','备注');

   if(!is_array($list)){
    $list[]=array('',$year.'学年','暂无数据','','','');//没有数据设置无数据提示行防止溢出死机
   }else{
      $list=listAddID($list);//增加自动排序序号
   }
    $data[1]=array(
      'sheetname'=>$year.'学年',
      'header'=>$year.'学年 学生获奖情况汇总',
      'title'=>$title,
      'width'=>$width,
      'data'=>$list,
      );


// dump($data);exit;
    $savefile=$year."学年学生获奖汇总".date(md);
    //输出文档函数在common/function.php中
    exportExcel($data,$savefile);

 }

  /*导出学生获奖情况汇总excel文件*/

     public function excel_1($year=""){
      $year=I('year')?I('year'):C('CUR_YEAR');
      $map["status"] =array('EGT',1);
      $map['year']=$year;

        $Model=$this->xshjModel;
        $order=array(
            'teacher'=>'asc',
            'sort' =>'asc',
          );
       
        $data=$Model->field('create_time,update_time,year',true)
                    ->where($map)->order($order)->select();

      // dump($data);exit;
       $count=count($data);
       $m=$count+2;//获取记录数+2
       //设置顶行标题
     $toptitle = $year.'学年 学生获奖情况汇总表';
      
        $xlsTitle = iconv('utf-8', 'gb2312', "学年学生获奖汇总");//设置文件名称
        $fileName = $year.$xlsTitle.date('_Ymd');//or $xlsTitle 文件名称可根据自己情况设定

        vendor("PHPExcel");  
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
    //设置当前工作表为$objActSheet
    $objActSheet = $objPHPExcel->getActiveSheet();
        $objActSheet ->setTitle(date(Y).'学年教师获奖汇总');//设置表标题 
        //$objActSheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); //设置为A4纸大小
        $objActSheet ->setCellValue('A1',$toptitle)->mergeCells('A1:H1');//mergeCells合并单元格   
        $objActSheet ->getStyle( 'A1')->getFont()->setSize(18);
    
    /*普通样式设置
    *设置表格线为细线--细边框BORDER_THICK边框是粗的*/
           $styleArray = array(  
                  'borders' => array(  
                     'allborders' => array(                 
                          'style' => \PHPExcel_Style_Border::BORDER_THIN,
                     ),
                   ),
                   'font' => array(
                    'bold' => false,
                    'color' => array('rgb' => '000000'),
                    'size' => 9,
                    'name' => '宋体'
                   ),
                 );

        $objActSheet ->getStyle('A2:H'.$m)->applyFromArray($styleArray );
        $objActSheet ->getStyle( 'A2')->getAlignment()->setShrinkToFit(true);//设置自动缩小字体填充;
        $objActSheet ->getStyle('A1:H2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
          ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
        $objActSheet ->getStyle( 'A1:H2')->getFont()->setBold(true);//设置顶端标题为粗体
        //设置单元格宽度
        $objActSheet ->getColumnDimension('A')->setWidth(5);//序号宽度
        $objActSheet ->getColumnDimension('B')->setWidth(8);//教师
        $objActSheet ->getColumnDimension('C')->setWidth(8);
        $objActSheet ->getColumnDimension('D')->setWidth(8);
        $objActSheet ->getColumnDimension('E')->setWidth(28);
        $objActSheet ->getColumnDimension('F')->setWidth(9);
        $objActSheet ->getColumnDimension('G')->setWidth(13);
        $objActSheet ->getColumnDimension('H')->setWidth(8);


        $objActSheet ->getStyle('A:A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
        $objActSheet ->getStyle('A:I')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);//设置水平、垂直居中
    //标题行设置
        $objActSheet ->getStyle('A2:I2')->getAlignment()->setWrapText(true);//设定为自动换行
        $objActSheet ->getStyle('D2')->getAlignment()->setWrapText(true);
        $objActSheet ->getStyle( 'A2:I2')->getFont()->setSize(9);
        $objActSheet ->setCellValue('A2',"序号");
        $objActSheet ->setCellValue('B2',"教师");
        $objActSheet ->setCellValue('C2',"获奖日期");
        $objActSheet ->setCellValue('D2',"获奖类别");
        $objActSheet ->setCellValue('E2',"获奖内容");
        $objActSheet ->setCellValue('F2',"获奖等级");
        $objActSheet ->setCellValue('G2',"举办单位");
        $objActSheet ->setCellValue('H2',"备注");
               
       //以下输出内容
        $baseRow = 3;      //指定插入到第3行后
        foreach($data as $k => $v){         
         $key=$k+1;         //$key是输出顺序号
        $num=$baseRow+$k; //$num是循环操作行的行号
        //Excel的第A 列，uid是你查出数组的键值，下面以此类推
        $objActSheet ->setCellValue('A'.$num, $key )
           ->setCellValue('B'.$num, ' '.$v['teacher'])
           ->setCellValue('C'.$num, $v['date'])
           ->setCellValue('D'.$num, $v['leibie'])
           ->setCellValue('E'.$num, $v['content'])
           ->setCellValue('F'.$num, $v['hjdj'])
           ->setCellValue('G'.$num, $v['jbdw'])
           ->setCellValue('H'.$num, $v['bz']);
          }
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        header("Content-Transfer-Encoding:binary");
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');         
        $objWriter->save('php://output'); 
        exit; 
        }

} 
