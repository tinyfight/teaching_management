<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-5-28
 * Time: 下午01:33
 * @author 水月居<zzl@ourstu.com>
 */

namespace Jxgl\Controller;

use Think\Controller;


class BaseController extends Controller{

    protected $jshjModel;
    protected $CateModel;

    function _initialize()
    {
        header("Content-Type:text/html;charset=utf-8"); 
        // if (!is_login()) {
        //     $this->error('本模块必须登录后才能使用。',U('ucenter/member/login'));
        // }

        $this->jshjModel = D('Jxgl/JxglJshj');        
        $this->CateModel = D('Jxgl/JxglJshjCategory');
        $this->xshjModel = D('Jxgl/JxglXshj');        
        $this->xsCateModel = D('Jxgl/JxglXshjCate');
        $this->lwfbModel=D('Jxgl/JxglJslwfb');

        $catTitle=modC('PAPER_CATEGORY_TITLE','教学管理','Jxgl');
        //$catTitle='学生招生管理';

        $sub_menu['left'][]= array('tab' => 'home', 'title' => $catTitle, 'href' =>  U('index'));
        $this->assign('sub_menu', $sub_menu);
        $this->assign('current','home');
        $this->assignSelf();
    }

    /**
     * assignSelf  输出当前登录用户信息
     * @author:xjw129xjt(肖骏涛) xjt@ourstu.com
     */
    private function assignSelf()
    {
        $self = query_user(array('title', 'avatar64', 'nickname','bj_code', 'uid', 'space_url', 'score', 'bj_name', 'fans', 'following', 'weibocount', 'rank_link'));
        //获取用户封面id
        $map = getUserConfigMap('user_cover');
        $map['role_id'] = 0;
        $model = D('Ucenter/UserConfig');
        $cover = $model->findData($map);
        $self['cover_id'] = $cover['value'];
        $self['cover_path'] = getThumbImageById($cover['value'], 273, 80);
        $this->assign('self', $self);
    }


} 