# 水月居教学管理平台

#### 介绍
基于opensns6开发学校管理平台，可用于学校教学管理，集成教师获奖登记、学生获奖登记、学生成绩管理统计（开发中....）【学生管理、课题管理、备课系统、五彩课程】（括号内模块不开放）。

#### 软件架构
软件架构说明：ZUI1.8.1+THINKPHP3.2.3+JQUERY+PHPexcel1.8+PHPword1.2


#### 安装教程

1. 输入网站地址，假设不在根目录下 如d:/www/teaching_management/  则输入 http://127.0.0.1/teaching_management/ 域名 http://www.zjttwgy.com/teaching_management/ 即可自动检测是否安装，运行安装程序，执行安装，安装完毕提示打开“前台/后台”。
2. 教学管理为Application下的独立模块，需在后台管理菜单中的模块管理下安装后才能在前台菜单中显示。
3. 后台管理中有插件管理与模块管理可用于模块、插件的安装与卸载。

#### 使用说明

1. 学校教学管理系统，属于个人二次开发系统，是基于opensns系统下开源系统，可以免费使用，但不授权用于商业用途。
2. 后台管理中增加了学期管理功能用于学期的新增，以及当前学期的设置。用户前台操作会自动读取当前学期信息，使用前需先正确设置学期信息。需要修改以前学期数据 ，需将当前学期信息切换至以前再修改数据。
3. 平台中的许多功能是基于学生管理模块的。如成绩登分表、学生信息导入与导出。

#### 更新说明
1. 升级原opensns中的zui1.2至1.81
2. 新增PHPexcel模块，支持系统数据库导出/导入excel文档，修改样式支持文档样式设计【表格线、表头字体、表头标题样式加粗】
3. 修复代码中的许多BUG，修改当项目文件不在根目录下底部友情链接图片不显示问题。

#### 参与贡献

1. Fork 本仓库
2. start 本仓库
3. 提交代码 git add . git commit -m "修改提示信息" git push
4. 拉取本仓库 Pull Request
5. 克隆至本地 git clone https://gitee.com/shuiyueju/teaching_management.git


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)