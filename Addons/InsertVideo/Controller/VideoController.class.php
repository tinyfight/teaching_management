<?php

namespace Addons\InsertVideo\Controller;
use Home\Controller\AddonsController;

class VideoController extends AddonsController{

    private $hosts = '';
    public function getVideoInfo()
    {

        $link = op_t(I('post.url'));

        if(preg_match("/.swf/i", $link, $check)){
            $return['boolen'] = 1;
            $return['is_swf'] = 1;
            $return['data'] = array('title'=>'无','flash_url'=>urldecode($link));
            exit(json_encode($return));
        }


        if (preg_match("/(youku.com|ku6.com|sohu.com|sina.com.cn|qq.com|tudou.com|yinyuetai.com|iqiyi.com)/i", $link, $hosts)) {
            $return['boolen'] = 1;
            $return['data'] = $link;
        } else {
            $return['boolen'] = 0;
            $return['message'] = '仅支持优酷、酷6、新浪、土豆网、搜狐、音悦台、腾讯、爱奇艺等视频发布';
        }
        $this->hosts = $hosts[1];
        $flashinfo = $this->getInfo($link);
        if (!$flashinfo['title'] || json_encode($flashinfo['title']) == 'null') {
            $flashinfo['title'] = "未获取标题";
        }
        $return['is_swf'] = 0;
        $return['data'] = $flashinfo;
        exit(json_encode($return));
    }

    private  function getInfo($link){
        require_once('./ThinkPHP/Library/Vendor/Collection/phpQuery.php');
        $host = $this->hosts;
        $content = get_content_by_url($link);
        if ('youku.com' == $host) {
            \phpQuery::newDocument($content);
            $title = pq("title")->html();
            $flash_url = pq("#link2")->attr('value');
        } elseif ('ku6.com' == $host) {
            \phpQuery::$defaultCharset=GBK;
            \phpQuery::newDocument($content);
            $title = pq("title")->html();
            $flash_url = pq(".ckl_input")->eq(0)->attr('value');
            $title = iconv("GBK", "UTF-8", $title);
        } elseif('tudou.com' == $host){
            \phpQuery::newDocument($content);
            $title = pq("title")->html();
            preg_match('/iid:(.*?)\s+,icode/s',$content,$program);
            $programId =  intval($program[1]);
            if(strpos($link, 'www.tudou.com/albumplay') !== false){
                preg_match("/albumplay\/([\w\-\.]+)[\/|\.]/", $link, $album);
                $albumId = $album[1];
                $flash_url = 'http://www.tudou.com/a/' . $albumId . '/&iid='.$programId.'/v.swf';
            }elseif(strpos($link, 'www.tudou.com/programs') !== false){
                $flash_url = 'http://www.tudou.com/v/' . $programId . '/v.swf';
            }elseif(strpos($link, 'www.tudou.com/listplay') !== false){
                preg_match("/listplay\/([\w\-\.]+)\//", $link, $list);
                $listId = $list[1];
                $flash_url = 'http://www.tudou.com/l/' . $listId . '/&iid='.$programId.'/v.swf';
            }
        }elseif ('sohu.com' == $host) {
           \phpQuery::$defaultCharset=GBK;
           \phpQuery::newDocument($content);
            $title = pq("title")->html();
            $title = iconv("GBK", "UTF-8", $title);
            $flash_url = pq("[property='og:videosrc']")->attr('content');
        } elseif ('qq.com' == $host) {

            $contentType ='text/html;charset=gbk';
            \phpQuery::newDocument($content,$contentType);
            preg_match("/vid=(.*)/i", $link, $vid);
            $vid = $vid[1];
            $flash_url = 'http://static.video.qq.com/TPout.swf?vid=' . $vid . '&auto=0';
            $title =   $title = pq("#".$vid)->attr('title');
        } elseif ('sina.com.cn' == $host) {
            \phpQuery::newDocument($content);
            $title = pq("title")->html();
            preg_match("/swfOutsideUrl:\'(.+?)\'/i", $content, $flashvar);
            $flash_url = $flashvar[1];

        } elseif ('yinyuetai.com' == $host) {
            \phpQuery::newDocument($content);
            $title = pq("title")->html();
            $flash_url = pq("[property='og:videosrc']")->attr('content');
        }elseif('iqiyi.com' == $host){
            \phpQuery::newDocument($content);
            $title = pq("title")->html();
            $obj =  pq("#videoArea")->find('div')->eq(0);
            $temp1 = $obj->attr('data-player-videoid');
            preg_match("/iqiyi.com\/(.*).html/i", $link, $temp2);
            $temp2 = $temp2[1];
            $temp3 = $obj->attr('data-player-albumid');
            $temp4 =$obj->attr('data-player-tvid');
            $flash_url ='http://player.video.qiyi.com/'.$temp1.'/0/0/'.$temp2.'.swf-albumId='.$temp3.'-tvId='.$temp4;
        }
        $return['title'] = text($title) ;
        $return['flash_url'] = urldecode($flash_url);
        return $return;
    }

}