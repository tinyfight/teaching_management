<?php
/**不支持5以上版本*/
namespace Addons\InsertVideo;

use Common\Controller\Addon;

class InsertVideoAddon extends Addon
{

    public $info = array(
        'name' => 'InsertVideo',
        'title' => '插入视频',
        'description' => '微博插入视频',
        'status' => 1,
        'author' => '駿濤',
        'version' => '0.1'
    );

    public function install()
    {
        return true;
    }

    public function uninstall()
    {
        return true;
    }

    //实现的InsertImage钩子方法
    public function weiboType($param)
    {
        $this->display('insertVideo');
    }



    public function fetchVideo($weibo)
    {
        $weibo_data = unserialize($weibo['data']);
        $this->assign('weibo',$weibo);
        $this->assign('weibo_data',$weibo_data);
        return $this->fetch('display');
    }


}